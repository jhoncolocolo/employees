<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return  void
     */
    public function run()
    {
        
        $this->call(CreateIdentificationTypeSeeder::class);
        $this->call(CreateAreaSeeder::class);
        $this->call(CreateCountrySeeder::class);
        
        \App\Models\Permission::factory(10)->create();
        
        \App\Models\Role::factory(10)->create();
        
        \App\Models\User::factory(50)->create();
        
        \App\Models\PermissionRole::factory(50)->create();
        
        \App\Models\PermissionUser::factory(50)->create();
    }
}