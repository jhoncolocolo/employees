<?php

namespace Database\Seeders;
use App\Models\IdentificationType;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\File;

class CreateIdentificationTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            $json = File::get("database/data/identification_types.json");
            $data = json_decode($json);
            foreach ($data as $obj) {
                IdentificationType::create([
                    "name"=> $obj->name
                ]);
            }
        } catch (\Illuminate\Contracts\Filesystem\FileNotFoundException $e) {
        }
    }
}