<?php

namespace Database\Seeders;
use App\Models\Area;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\File;

class CreateAreaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        try {
            $json = File::get("database/data/areas.json");
            $data = json_decode($json);
            foreach ($data as $obj) {
                Area::create([
                    "name"=> $obj->name
                ]);
            }
        } catch (\Illuminate\Contracts\Filesystem\FileNotFoundException $e) {
        }
    }
}