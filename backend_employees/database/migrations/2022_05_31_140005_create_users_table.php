<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB; 
return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return    void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('area_id')->comment("Areas Table Id ");
            $table->unsignedBigInteger('country_id')->comment("Countries Table Id ");
            $table->unsignedBigInteger('identification_type_id')->comment("Identification_types Table Id ");
            $table->string('identification_number',20)->comment("User's Table identification Number");
            $table->string('user_name',255)->comment("User's Table user_name");
            $table->string('email',300)->unique()->comment("User's Table email");
            $table->string('password',255)->nullable()->comment("User's Table password");
            $table->string('first_name',20)->comment("User's Table first_name");
            $table->string('other_names',50)->nullable()->comment("User's Table Other Names");
            $table->string('surname',20)->comment("User's Table surname");
            $table->string('second_surname',20)->comment("User's Table second_surname");
            $table->date('register')->comment("User's Table register");
            $table->enum('status',array('0','1'))->nullable()->default('1')->comment("User's status 0 is equals to inactive,  and 1 is equals to active.");
            $table->timestamp('email_verified_at')->nullable()->comment("User's Table email_verified_at");
            $table->string('remember_token',100)->nullable()->comment("User's Table remember_token");
            $table->timestamps();
    
            $table->foreign('area_id')
                ->references('id')
                ->on('areas')
                ->onUpdate('cascade')
                ->onDelete('cascade');
    
            $table->foreign('country_id')
                ->references('id')
                ->on('countries')
                ->onUpdate('cascade')
                ->onDelete('cascade');
    
            $table->foreign('identification_type_id')
                ->references('id')
                ->on('identification_types')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        
        });
         DB::statement("ALTER TABLE `users` comment 'Table´s Users of system'");
    }

    /**
     * Reverse the migrations.
     *
     * @return    void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
};