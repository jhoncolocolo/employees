<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB; 
return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return    void
     */
    public function up()
    {
        Schema::create('identification_types', function (Blueprint $table) {
            $table->id();
            $table->string('name',25)->comment("Identification_type's Table name");
            $table->timestamps();
        
        });
         DB::statement("ALTER TABLE `identification_types` comment 'Indentification Types Table'");
    }

    /**
     * Reverse the migrations.
     *
     * @return    void
     */
    public function down()
    {
        Schema::dropIfExists('identification_types');
    }
};