<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB; 
return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return    void
     */
    public function up()
    {
        Schema::create('permissions', function (Blueprint $table) {
            $table->id();
            $table->string('name',255)->comment("Permission's Table name");
            $table->string('route',255)->unique()->comment("Permission's Table route");
            $table->string('path',255)->nullable()->comment("Permission's Table path");
            $table->mediumText('description')->nullable()->comment("Permission's Table description");
            $table->timestamps();
        
        });
         DB::statement("ALTER TABLE `permissions` comment 'Table&quot;s Permissions'");
    }

    /**
     * Reverse the migrations.
     *
     * @return    void
     */
    public function down()
    {
        Schema::dropIfExists('permissions');
    }
};