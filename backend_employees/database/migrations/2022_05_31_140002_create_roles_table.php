<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB; 
return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return    void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->id();
            $table->string('name',255)->comment("Role's Table name");
            $table->string('role',120)->nullable()->comment("Role's Table role");
            $table->mediumText('description')->nullable()->comment("Role's Table description");
            $table->timestamps();
        
        });
         DB::statement("ALTER TABLE `roles` comment 'Roles for Allow Access in the Any System'");
    }

    /**
     * Reverse the migrations.
     *
     * @return    void
     */
    public function down()
    {
        Schema::dropIfExists('roles');
    }
};