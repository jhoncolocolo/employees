<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Permission>
*/
class PermissionFactory extends Factory
{ 
 
    /**
     * Define the model's default state.
     *
     * @return  array
     */
    public function definition()
    {   
        return[
            'name' => $this->faker->text(255),
            'route' => $this->faker->text(255),
            'path' => $this->faker->text(255),
            'description' => $this->faker->sentence(),
        ];
    }
}