<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Permission;
use App\Models\User;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\PermissionUser>
*/
class PermissionUserFactory extends Factory
{ 
 
    /**
     * Define the model's default state.
     *
     * @return  array
     */
    public function definition()
    {   
        return[
            'permission_id' => Permission::inRandomOrder()->first()->id,
            'user_id' => User::inRandomOrder()->first()->id,
        ];
    }
}