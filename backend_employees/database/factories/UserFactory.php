<?php

namespace Database\Factories;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Area;
use App\Models\Country;
use App\Models\IdentificationType;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\User>
*/
class UserFactory extends Factory
{ 
 
    /**
     * Define the model's default state.
     *
     * @return  array
     */
    public function definition()
    {   
        $country = Country::inRandomOrder()->first(); 
        $first_name = strtoupper(\General::replaceNonLetters($this->faker->firstName(20)));
        $last_name = strtoupper(\General::replaceNonLetters($this->faker->lastName(20)));
        return[
            'area_id' => Area::inRandomOrder()->first()->id,
            'country_id' => $country->id,
            'identification_type_id' => IdentificationType::inRandomOrder()->first()->id,
            'identification_number' => $this->faker->ein,
            'user_name' => \User::getCredentialByFirstNameAndSurName($first_name, $last_name),
            'email' => \User::getCredentialByFirstNameAndSurName($first_name, $last_name,$country->id ),
            'password' => '$2y$10$NdKdOBqdcInc/w3Bvu8JPObhxpbiKZxQN0V/2vR6bDFsJwg7LdwCm',
            'first_name' => $first_name,
            'other_names' => strtoupper(\General::replaceNonLetters($this->faker->firstName(50))),
            'surname' => $last_name,
            'second_surname' => strtoupper(\General::replaceNonLetters($this->faker->lastName(20))),
            'register' => $this->faker->date($format = 'Y-m-d', $max = 'now'),
            'status' => $this->faker->randomElement(array('0','1')),
            'email_verified_at' => $this->faker->dateTime($max = 'now')->format('Y-m-d H:i:s'),
            'remember_token' => Str::random(10),
        ];
    }
}