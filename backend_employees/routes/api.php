<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::resource('users', 'App\Http\Controllers\UserController');
Route::resource('permissions', 'App\Http\Controllers\PermissionController');
Route::resource('roles', 'App\Http\Controllers\RoleController');
Route::resource('permission_roles', 'App\Http\Controllers\PermissionRoleController');
Route::resource('permission_users', 'App\Http\Controllers\PermissionUserController');
Route::resource('identification_types', 'App\Http\Controllers\IdentificationTypeController');
Route::resource('countries', 'App\Http\Controllers\CountryController');
Route::resource('areas', 'App\Http\Controllers\AreaController');