<?php

namespace App\Services\IdentificationType;
use Illuminate\Support\Facades\Facade;


class IdentificationTypeFacade extends Facade
{
    protected static function getFacadeAccessor(){
        return 'IdentificationType';
    }
}