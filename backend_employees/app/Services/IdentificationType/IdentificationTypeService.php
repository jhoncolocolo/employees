<?php

namespace App\Services\IdentificationType; 
use App\Repositories\IdentificationType\IdentificationTypeRepository;

class IdentificationTypeService
{ 
    /**
    *Return all values
     *
     * @return  mixed
    */
    public function all()
	{
      return (new IdentificationTypeRepository())->all();
    }


   /*
    * Get IdentificationType By Id
    * @param  $identificationTypeId Int
    */
    public function find($identificationTypeId)
    {
        return (new IdentificationTypeRepository())->show($identificationTypeId);
    }

    /*
    * Do IdentificationType
    * @param  $data Array
    */
    public function store($data)
    {
        //Save IdentificationType
        return (new IdentificationTypeRepository())->store($data);
    }

    /*
    * Update IdentificationType
    * @param  $identificationTypeId Int
    * @param  $data Array
    */
    public function update($identificationTypeId, $data)
    {
        //Update IdentificationType
        return (new IdentificationTypeRepository())->update($identificationTypeId, $data);
    }

    /*
    * Delete IdentificationType
    * @param  $identificationTypeId Int
    */
    public function destroy($identificationTypeId)
    {
        //Delete IdentificationType
        return (new IdentificationTypeRepository())->destroy($identificationTypeId);
    }
}