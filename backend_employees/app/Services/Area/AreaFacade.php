<?php

namespace App\Services\Area;
use Illuminate\Support\Facades\Facade;


class AreaFacade extends Facade
{
    protected static function getFacadeAccessor(){
        return 'Area';
    }
}