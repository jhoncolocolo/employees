<?php

namespace App\Services\Area; 
use App\Repositories\Area\AreaRepository;

class AreaService
{ 
    /**
    *Return all values
     *
     * @return  mixed
    */
    public function all()
	{
      return (new AreaRepository())->all();
    }


   /*
    * Get Area By Id
    * @param  $areaId Int
    */
    public function find($areaId)
    {
        return (new AreaRepository())->show($areaId);
    }

    /*
    * Do Area
    * @param  $data Array
    */
    public function store($data)
    {
        //Save Area
        return (new AreaRepository())->store($data);
    }

    /*
    * Update Area
    * @param  $areaId Int
    * @param  $data Array
    */
    public function update($areaId, $data)
    {
        //Update Area
        return (new AreaRepository())->update($areaId, $data);
    }

    /*
    * Delete Area
    * @param  $areaId Int
    */
    public function destroy($areaId)
    {
        //Delete Area
        return (new AreaRepository())->destroy($areaId);
    }
}