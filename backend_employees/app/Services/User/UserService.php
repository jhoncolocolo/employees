<?php

namespace App\Services\User; 
use App\Repositories\User\UserRepository;

class UserService
{ 
    /**
    *Return all values
     *
     * @return  mixed
    */
    public function all()
	{
      return (new UserRepository())->all();
    }

    /**
     * Get Users By First Name And SurName
     * $first_name | String | Required 
     * $last_name | String | Required 
     * $country | Int | Optional | For get User name with domain 
     * @return  mixed
    */
    public function getCredentialByFirstNameAndSurName($first_name,$surname,$country=null ){
        $whithDomain = ( !is_null($country) ? '@cidenet.com.'.($country == 1 ? 'co':'us')  : '' );
        $matches = (new UserRepository())->getUsersByFirstNameAndSurName($first_name,$surname);
        $consecutive = "";
        if(count($matches) > 0){
            foreach ($matches as $key => $user) {
                $number_email = preg_replace('/[^0-9]/', '', $user->user_name);
                $consecutive = ($number_email != "" ? $number_email : 0 );
            }
            $consecutive= intval($consecutive) + 1;
        }
        return str_replace(' ', '', strtolower($first_name).".".strtolower($surname).$consecutive.$whithDomain);
    }

    /**
    *Return all Registries With Detail
     *
     * @return  mixed
    */
    public function allDetails($typeParam = null,$value=null)
    {
      return (new UserRepository())->allDetails($typeParam,$value);
    } 

   /*
    * Get User By Id
    * @param  $userId Int
    */
    public function find($userId)
    {
        return (new UserRepository())->show($userId);
    }

    /*
    * Do User
    * @param  $data Array
    */
    public function store($data)
    {
        $data['email'] = $this->getCredentialByFirstNameAndSurName($data['first_name'], $data['surname'],$data['country_id']);
        $data['user_name'] = $this->getCredentialByFirstNameAndSurName($data['first_name'], $data['surname']);
        //Save User
        return (new UserRepository())->store($data);
    }

    /*
    * Update User
    * @param  $userId Int
    * @param  $data Array
    */
    public function update($userId, $data)
    {   $userDB= (new UserRepository())->show($userId);
        if($userDB->first_name !=  $data["first_name"] || $userDB->surname !=  $data["surname"] ||  $userDB->country_id !=  $data["country_id"]){
            $data['email'] = $this->getCredentialByFirstNameAndSurName($data['first_name'], $data['surname'],$data['country_id']);
            $data['user_name'] = $this->getCredentialByFirstNameAndSurName($data['first_name'], $data['surname']);
        }
        //Update User
        return (new UserRepository())->update($userId, $data);
    }

    /*
    * Delete User
    * @param  $userId Int
    */
    public function destroy($userId)
    {
        //Delete User
        return (new UserRepository())->destroy($userId);
    }
}