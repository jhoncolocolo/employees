<?php

namespace App\Services\Country; 
use App\Repositories\Country\CountryRepository;

class CountryService
{ 
    /**
    *Return all values
     *
     * @return  mixed
    */
    public function all()
	{
      return (new CountryRepository())->all();
    }


   /*
    * Get Country By Id
    * @param  $countryId Int
    */
    public function find($countryId)
    {
        return (new CountryRepository())->show($countryId);
    }

    /*
    * Do Country
    * @param  $data Array
    */
    public function store($data)
    {
        //Save Country
        return (new CountryRepository())->store($data);
    }

    /*
    * Update Country
    * @param  $countryId Int
    * @param  $data Array
    */
    public function update($countryId, $data)
    {
        //Update Country
        return (new CountryRepository())->update($countryId, $data);
    }

    /*
    * Delete Country
    * @param  $countryId Int
    */
    public function destroy($countryId)
    {
        //Delete Country
        return (new CountryRepository())->destroy($countryId);
    }
}