<?php

namespace App\Services\Country;
use Illuminate\Support\Facades\Facade;


class CountryFacade extends Facade
{
    protected static function getFacadeAccessor(){
        return 'Country';
    }
}