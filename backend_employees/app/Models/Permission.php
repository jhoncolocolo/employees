<?php

namespace App\Models;
 
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany; 
 

class Permission extends Model
{ 
  use HasFactory;
	protected $table = 'permissions';
 
	protected $primaryKey = 'id';
 
	protected $fillable = ["name","route","path","description"];
 
	protected $hidden = ['created_at','updated_at'];

  /**
   * @return  mixed
  */
  public function permission_role(): HasMany
  {
      return $this->hasMany(PermissionRole::class);
  }

  /**
   * @return  mixed
  */
  public function permission_user(): HasMany
  {
      return $this->hasMany(PermissionUser::class);
  }

	
 
}