<?php

namespace App\Models;
 
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany; 
use Illuminate\Database\Eloquent\Relations\BelongsTo; 
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Model
{ 
  use HasFactory;
  use HasApiTokens, Notifiable;
	protected $table = 'users';
 
	protected $primaryKey = 'id';
 
	protected $fillable = ["area_id","country_id","identification_type_id","identification_number","user_name","email","password","first_name","other_names","surname","second_surname","register","status","email_verified_at","remember_token"];
 
	protected $hidden = ['password','remember_token','email_verified_at','created_at','updated_at'];

  /**
   * @return  mixed
  */
  public function permission_user(): HasMany
  {
      return $this->hasMany(PermissionUser::class);
  }

	
  /**
   * @return  mixed
  */
  public function area(): BelongsTo
  {
      return $this->belongsTo(Area::class);
  }

  /**
   * @return  mixed
  */
  public function country(): BelongsTo
  {
      return $this->belongsTo(Country::class);
  }

  /**
   * @return  mixed
  */
  public function identification_type(): BelongsTo
  {
      return $this->belongsTo(IdentificationType::class);
  }

 
}