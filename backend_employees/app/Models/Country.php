<?php

namespace App\Models;
 
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany; 
 

class Country extends Model
{ 
  use HasFactory;
	protected $table = 'countries';
 
	protected $primaryKey = 'id';
 
	protected $fillable = ["name"];
 
	protected $hidden = ['created_at','updated_at'];

  /**
   * @return  mixed
  */
  public function user(): HasMany
  {
      return $this->hasMany(User::class);
  }

	
 
}