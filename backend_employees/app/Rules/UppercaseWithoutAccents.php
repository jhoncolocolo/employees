<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class UppercaseWithoutAccents implements Rule
{
    private $is_mandatory;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($is_mandatory = true)
    {
        $this->is_mandatory = $is_mandatory;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return (strtoupper($value) === $value && ! \General::isUnicode($value) );
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'El :attribute '.($this->is_mandatory ? 'es obligatorio' : '' ).', debe tener sólo mayúsculas y no tener acentos';
    }
}
