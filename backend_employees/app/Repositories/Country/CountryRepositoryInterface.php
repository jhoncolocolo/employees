<?php

namespace App\Repositories\Country;

interface CountryRepositoryInterface { 
   public function all();
   public function show($country);
   public function store($data);
   public function update($country,$data);
   public function destroy($country);
}