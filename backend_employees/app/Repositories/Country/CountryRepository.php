<?php

namespace App\Repositories\Country; 
use Carbon\Carbon;
use Illuminate\Support\Facades\DB; 
use App\Models\Country;

class CountryRepository  implements CountryRepositoryInterface
{ 
   /**
    *Return all values
     *
     * @return  mixed
   */
   public function all()
	 {
      return Country::all();
   }
   
    /**
    *Display the specified resource.
     *
     * @return  \Illuminate\Http\Response
   */
   public function show($country)
    {
      return Country::find($country);
   }

   /**
    * Save Country
     *
     * @return  mixed
   */
    public function store($data)
    {
      return Country::create(array(
        'name' => $data['name'],
        'created_at' => Carbon::now()
      ));
    }

   /**
    *Update Country
     *
     * @return  mixed
   */
   public function update($country,$data)
     {
      //Find Country
      $country = Country::find($country);
      $country->fill(array(
        'name' => $data['name'],
        'updated_at' => Carbon::now()
      ));
      return $country->save();
   }


   /**
    *Delete Country
     *
     * @return  mixed
   */
   public function destroy($country)
     {
      //Find Country
      $country = Country::find($country);
      return $country->delete();
   }

}