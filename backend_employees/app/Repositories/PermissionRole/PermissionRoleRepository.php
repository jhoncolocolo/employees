<?php

namespace App\Repositories\PermissionRole; 
use Carbon\Carbon;
use Illuminate\Support\Facades\DB; 
use App\Models\PermissionRole;

class PermissionRoleRepository  implements PermissionRoleRepositoryInterface
{ 
   /**
    *Return all values
     *
     * @return  mixed
   */
   public function all()
	 {
      return PermissionRole::all();
   }
   /*
    * Return all Detail
    * @return  mixed
   */
   public function allDetails($search)
   { 
     $select =PermissionRole::select( 
                  'permission_roles.id',   
                  'permission_roles.permission_id',   
                  'permission_roles.role_id' 
                  )     
                ->join('permissions','permission_roles.permission_id','=','permissions.id')    
                ->join('roles','permission_roles.role_id','=','roles.id') ;   
                if($search != null){
                    $select 
                      ->orWhereRaw("permissions.id LIKE '%".$search."%'") 
                      ->orWhereRaw("roles.id LIKE '%".$search."%'"); 
                }

     return $select->get();
   }
   
    /**
    *Display the specified resource.
     *
     * @return  \Illuminate\Http\Response
   */
   public function show($permissionRole)
    {
      return PermissionRole::find($permissionRole);
   }

   /**
    * Save PermissionRole
     *
     * @return  mixed
   */
    public function store($data)
    {
      return PermissionRole::create(array(
        'permission_id' => $data['permission_id'],
        'role_id' => $data['role_id'],
        'created_at' => Carbon::now()
      ));
    }

   /**
    *Update PermissionRole
     *
     * @return  mixed
   */
   public function update($permissionRole,$data)
     {
      //Find PermissionRole
      $permissionRole = PermissionRole::find($permissionRole);
      $permissionRole->fill(array(
        'permission_id' => $data['permission_id'],
        'role_id' => $data['role_id'],
        'updated_at' => Carbon::now()
      ));
      return $permissionRole->save();
   }


   /**
    *Delete PermissionRole
     *
     * @return  mixed
   */
   public function destroy($permissionRole)
     {
      //Find PermissionRole
      $permissionRole = PermissionRole::find($permissionRole);
      return $permissionRole->delete();
   }

}