<?php

namespace App\Repositories\IdentificationType;

interface IdentificationTypeRepositoryInterface { 
   public function all();
   public function show($identificationType);
   public function store($data);
   public function update($identificationType,$data);
   public function destroy($identificationType);
}