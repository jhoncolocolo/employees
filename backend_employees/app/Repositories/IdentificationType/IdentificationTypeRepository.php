<?php

namespace App\Repositories\IdentificationType; 
use Carbon\Carbon;
use Illuminate\Support\Facades\DB; 
use App\Models\IdentificationType;

class IdentificationTypeRepository  implements IdentificationTypeRepositoryInterface
{ 
   /**
    *Return all values
     *
     * @return  mixed
   */
   public function all()
	 {
      return IdentificationType::all();
   }
   
    /**
    *Display the specified resource.
     *
     * @return  \Illuminate\Http\Response
   */
   public function show($identificationType)
    {
      return IdentificationType::find($identificationType);
   }

   /**
    * Save IdentificationType
     *
     * @return  mixed
   */
    public function store($data)
    {
      return IdentificationType::create(array(
        'name' => $data['name'],
        'created_at' => Carbon::now()
      ));
    }

   /**
    *Update IdentificationType
     *
     * @return  mixed
   */
   public function update($identificationType,$data)
     {
      //Find IdentificationType
      $identificationType = IdentificationType::find($identificationType);
      $identificationType->fill(array(
        'name' => $data['name'],
        'updated_at' => Carbon::now()
      ));
      return $identificationType->save();
   }


   /**
    *Delete IdentificationType
     *
     * @return  mixed
   */
   public function destroy($identificationType)
     {
      //Find IdentificationType
      $identificationType = IdentificationType::find($identificationType);
      return $identificationType->delete();
   }

}