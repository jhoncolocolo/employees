<?php

namespace App\Repositories\Area; 
use Carbon\Carbon;
use Illuminate\Support\Facades\DB; 
use App\Models\Area;

class AreaRepository  implements AreaRepositoryInterface
{ 
   /**
    *Return all values
     *
     * @return  mixed
   */
   public function all()
	 {
      return Area::all();
   }
   
    /**
    *Display the specified resource.
     *
     * @return  \Illuminate\Http\Response
   */
   public function show($area)
    {
      return Area::find($area);
   }

   /**
    * Save Area
     *
     * @return  mixed
   */
    public function store($data)
    {
      return Area::create(array(
        'name' => $data['name'],
        'created_at' => Carbon::now()
      ));
    }

   /**
    *Update Area
     *
     * @return  mixed
   */
   public function update($area,$data)
     {
      //Find Area
      $area = Area::find($area);
      $area->fill(array(
        'name' => $data['name'],
        'updated_at' => Carbon::now()
      ));
      return $area->save();
   }


   /**
    *Delete Area
     *
     * @return  mixed
   */
   public function destroy($area)
     {
      //Find Area
      $area = Area::find($area);
      return $area->delete();
   }

}