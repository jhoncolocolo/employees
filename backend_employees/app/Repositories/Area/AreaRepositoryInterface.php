<?php

namespace App\Repositories\Area;

interface AreaRepositoryInterface { 
   public function all();
   public function show($area);
   public function store($data);
   public function update($area,$data);
   public function destroy($area);
}