<?php

namespace App\Repositories\User; 
use Carbon\Carbon;
use Illuminate\Support\Facades\DB; 
use App\Models\User;
use Illuminate\Support\Str;

class UserRepository  implements UserRepositoryInterface
{ 
   /**
    *Return all values
     *
     * @return  mixed
   */
   public function all()
	 {
      return User::all();
   }
   /*
    * Return all Detail
    * @return  mixed
   */
   public function allDetails($typeParam = null,$value=null )
   { 
     $select =User::select( 
                  'users.id',   
                  'areas.name AS area_company',   
                  'countries.name AS country',   
                  'identification_types.name AS identification_type',   
                  'users.identification_number AS identification',    
                  'users.email',   
                  DB::raw("CONCAT_WS(CONVERT( ' ' USING latin1),users.first_name,users.other_names,users.surname,users.second_surname) AS employee"),
                  'users.register AS register',   
                  DB::raw("CASE WHEN users.status = '0' THEN 'inactive' WHEN users.status = '1' THEN 'active' END AS status")
                  )     
                ->join('areas','users.area_id','=','areas.id')    
                ->join('countries','users.country_id','=','countries.id')    
                ->join('identification_types','users.identification_type_id','=','identification_types.id') ;  
                if($typeParam && $value){
                  switch ($typeParam) {
                    case 'first_name':
                      $select->whereRaw("first_name LIKE '%".$value."%'");
                      break;
                    case 'other_names':
                       $select->whereRaw("other_names LIKE '%".$value."%'");
                      break;
                    case 'surname':
                       $select->whereRaw("surname LIKE '%".$value."%'");
                      break;
                    case 'second_surname':
                       $select->whereRaw("second_surname LIKE '%".$value."%'");
                      break;
                    case 'identification_type':
                      $select->whereRaw("identification_types.name LIKE '%".$value."%'");
                      break;
                    case 'identification':
                      $select->whereRaw("identification LIKE '%".$value."%'");
                      break;
                    case 'email':
                      $select->whereRaw("email LIKE '%".$value."%'");
                      break;
                    case 'country':
                      $select->whereRaw("countries.name LIKE '%".$value."%'");
                      break;
                    case 'status':
                      $select->whereRaw("status LIKE '%".$value."%'");
                      break;
                  }
                }
                
                /*$resultado['Parametros'] = $select->getBindings();
        $query = str_replace(array('%', '?'), array('%%', '%s'), $select->toSql());
        $query = vsprintf($query, $select->getBindings());
        $resultado['Consulta'] = $query;
            dd(  $resultado);*/
     return $select
     ->paginate(10)
     //->get()
     ;
   }
   
    /**
    *Display the specified resource.
     *
     * @return  \Illuminate\Http\Response
   */
   public function show($user)
    {
      return User::find($user);
   }

   /**
    * Save User
     *
     * @return  mixed
   */
    public function store($data)
    {
      return User::create(array(
        'area_id' => $data['area_id'],
        'country_id' => $data['country_id'],
        'identification_type_id' => $data['identification_type_id'],
        'identification_number' => $data['identification_number'],
        'user_name' => $data['user_name'],
        'email' => $data['email'],
        'password' => '$2y$10$NdKdOBqdcInc/w3Bvu8JPObhxpbiKZxQN0V/2vR6bDFsJwg7LdwCm',
        'first_name' => $data['first_name'],
        'other_names' => $data['other_names'],
        'surname' => $data['surname'],
        'second_surname' => $data['second_surname'],
        'register' => $data['register'],
        'status' => '1',
        'email_verified_at' => now(),
        'remember_token' => Str::random(10),
        'created_at' => Carbon::now()
      ));
    }

   /**
    *Update User
     *
     * @return  mixed
   */
   public function update($user,$data)
     {
      //Find User
      $user = User::find($user);
      $user->fill(array(
        'area_id' => $data['area_id'],
        'country_id' => $data['country_id'],
        'identification_type_id' => $data['identification_type_id'],
        'identification_number' => $data['identification_number'],
        'user_name' => ($data['user_name']) ? $data['user_name'] : $user->user_name,
        'email' => ($data['email']) ? $data['email'] : $user->email,
        'first_name' => $data['first_name'],
        'other_names' => $data['other_names'],
        'surname' => $data['surname'],
        'second_surname' => $data['second_surname'],
        'status' => $data['status'],
        'updated_at' => Carbon::now()
      ));
      return $user->save();
   }


   /**
    *Delete User
     *
     * @return  mixed
   */
   public function destroy($user)
     {
      //Find User
      $user = User::find($user);
      return $user->delete();
   }


   /**
     * Get Users By First Name And SurName
     * @param $first_name 
     * @param $surname
     * @return  mixed
    */
    public function getUsersByFirstNameAndSurName($first_name,$surname){
        return User::whereFirstName($first_name,$surname)
                    ->whereSurname($surname)
                    ->get();
    }

}