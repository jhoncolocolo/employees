<?php

namespace App\Repositories\User;

interface UserRepositoryInterface { 
   public function all();
   public function allDetails($typeParam,$value);
   public function show($user);
   public function store($data);
   public function update($user,$data);
   public function destroy($user);
   public function getUsersByFirstNameAndSurName($first_name,$surname);
}