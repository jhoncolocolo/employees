<?php

namespace App\Providers;

use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return  void
     */
    public function boot()
    {
    }

    /**
     * Register the application services.
     *
     * @return  void
     */
    public function register()
    {
            App::bind(
            'App\Repositories\User\UserRepositoryInterface',
            'App\Repositories\User\UserRepository'
        );
        App::bind(
            'App\Repositories\Permission\PermissionRepositoryInterface',
            'App\Repositories\Permission\PermissionRepository'
        );
        App::bind(
            'App\Repositories\Role\RoleRepositoryInterface',
            'App\Repositories\Role\RoleRepository'
        );
        App::bind(
            'App\Repositories\PermissionRole\PermissionRoleRepositoryInterface',
            'App\Repositories\PermissionRole\PermissionRoleRepository'
        );
        App::bind(
            'App\Repositories\PermissionUser\PermissionUserRepositoryInterface',
            'App\Repositories\PermissionUser\PermissionUserRepository'
        );
        App::bind(
            'App\Repositories\IdentificationType\IdentificationTypeRepositoryInterface',
            'App\Repositories\IdentificationType\IdentificationTypeRepository'
        );
        App::bind(
            'App\Repositories\Country\CountryRepositoryInterface',
            'App\Repositories\Country\CountryRepository'
        );
        App::bind(
            'App\Repositories\Area\AreaRepositoryInterface',
            'App\Repositories\Area\AreaRepository'
        );
    }
}