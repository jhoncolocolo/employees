<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\General\GeneralService;
use App\Services\User\UserService;
use App\Services\Permission\PermissionService;
use App\Services\Role\RoleService;
use App\Services\PermissionRole\PermissionRoleService;
use App\Services\PermissionUser\PermissionUserService;
use App\Services\IdentificationType\IdentificationTypeService;
use App\Services\Country\CountryService;
use App\Services\Area\AreaService;

class GeneralServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return  void
     */
    public function boot()
    {
    }

    /**
     * Register the application services.
     *
     * @return  void
     */
    public function register()
    {
        $this->app->bind('General', function () {
            return new GeneralService();
        });
            $this->app->bind('User', function () {
            return new UserService();
        });
        $this->app->bind('Permission', function () {
            return new PermissionService();
        });
        $this->app->bind('Role', function () {
            return new RoleService();
        });
        $this->app->bind('PermissionRole', function () {
            return new PermissionRoleService();
        });
        $this->app->bind('PermissionUser', function () {
            return new PermissionUserService();
        });
        $this->app->bind('IdentificationType', function () {
            return new IdentificationTypeService();
        });
        $this->app->bind('Country', function () {
            return new CountryService();
        });
        $this->app->bind('Area', function () {
            return new AreaService();
        });
    }
}