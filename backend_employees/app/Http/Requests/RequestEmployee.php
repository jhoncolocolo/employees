<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\ValidationException;
use App\Rules\UppercaseWithoutAccents;
use App\Rules\IdentificationValidation;

class RequestEmployee extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => ['required','max:20',new UppercaseWithoutAccents()],
            'other_names' => ['max:50',new UppercaseWithoutAccents(false)],
            'surname' => ['required','max:20',new UppercaseWithoutAccents()],
            'second_surname' => ['required','max:20',new UppercaseWithoutAccents()],
            'register' => ['required', 'before:'.now()->toDateString(), 'after:'.now()->subMonths(1)->toDateString()],
            'identification_number' => [
                'required', 
                'unique:users,identification_number',
                new IdentificationValidation()
            ],

        ];
    }

    public function messages(){
        return [
            'register.before' => 'La fecha no puede ser superior al día actual',
            'register.after' => 'La fecha no puede ser inferior al día '.now()->subMonths(1)->toDateString()
        ];
    }

    protected function failedValidation(Validator $validator){
        $errors = (new ValidationException($validator))->errors();
        throw new HttpResponseException(
            response()->json( 
                ['message' => 'Validation exception',
                'errors' => $errors ], 405
            )
        );
    }
}