<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\ValidationException;
use App\Rules\UppercaseWithoutAccents;
use App\Rules\IdentificationValidation;
use Illuminate\Validation\Rule;

class RequestEmployeeUpdate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => ['required','max:20',new UppercaseWithoutAccents()],
            'other_names' => ['max:50',new UppercaseWithoutAccents(false)],
            'surname' => ['required','max:20',new UppercaseWithoutAccents()],
            'second_surname' => ['required','max:20',new UppercaseWithoutAccents()],
        ];
    }

    protected function failedValidation(Validator $validator){
        $errors = (new ValidationException($validator))->errors();
        throw new HttpResponseException(
            response()->json( 
                ['message' => 'Validation exception',
                'errors' => $errors ], 405
            )
        );
    }
}