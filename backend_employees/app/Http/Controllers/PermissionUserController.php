<?php

namespace App\Http\Controllers; 
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PermissionUserController extends Controller 
{
    
    /**
     * Display a listing of the resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(\PermissionUser::all());
    }

    /**
     * Display the specified resource.
     *
     * @param    \App\Models\PermissionUser  $PermissionUser
     * @return  \Illuminate\Http\Response
     */
    public function show($id)
    {
        $permissionUser = \PermissionUser::find($id);
        return response()->json($permissionUser);
    } 
 
    /*
    * Store PermissionUser
    * @return  void
    */    
    public function store(Request $request)
     {
       //Save permissionUsers
       $permissionUser = \PermissionUser::store($request);
       $data = [];
       if ($permissionUser) {
           $data['successful'] = true;
           $data['message'] = 'Record Entered Successfully';
           $data['last_insert_id'] = $permissionUser->id;
       }else{
           $data['successful'] = false;
           $data['message'] = 'Record Not Entered Successfully';
       }
       return response()->json($data);
  }

    /*
    * Update PermissionUser
    * @return  void
    */ 
    public function update($permissionUser,Request $request)
     {
       //Update permissionUsers
       $permissionUser = \PermissionUser::update($permissionUser, $request);
       $data = [];
       if ($permissionUser) {
           $data['successful'] = true;
           $data['message'] = 'Record Update Successfully';
           $data['permission_user_id'] = $permissionUser;
       }else{
           $data['successful'] = false;
           $data['message'] = 'Record Not Update Successfully';
       }
       return response()->json($data);
    }

    /*
    * Delete $permissionUser
    * @return  void
    */ 
    public function destroy($permissionUser)
     {
       //Delete permissionUsers
       $permissionUser = \PermissionUser::destroy($permissionUser);
       $data = [];
       if ($permissionUser) {
           $data['successful'] = true;
           $data['message'] = 'Record Delete Successfully';

       }else{
           $data['successful'] = false;
           $data['message'] = 'Record Not Delete Successfully';
       }
       return response()->json($data);
    }
}