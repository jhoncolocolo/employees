<?php

namespace App\Http\Controllers; 
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class RoleController extends Controller 
{
    
    /**
     * Display a listing of the resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(\Role::all());
    }

    /**
     * Display the specified resource.
     *
     * @param    \App\Models\Role  $Role
     * @return  \Illuminate\Http\Response
     */
    public function show($id)
    {
        $role = \Role::find($id);
        return response()->json($role);
    } 
 
    /*
    * Store Role
    * @return  void
    */    
    public function store(Request $request)
     {
       //Save roles
       $role = \Role::store($request);
       $data = [];
       if ($role) {
           $data['successful'] = true;
           $data['message'] = 'Record Entered Successfully';
           $data['last_insert_id'] = $role->id;
       }else{
           $data['successful'] = false;
           $data['message'] = 'Record Not Entered Successfully';
       }
       return response()->json($data);
  }

    /*
    * Update Role
    * @return  void
    */ 
    public function update($role,Request $request)
     {
       //Update roles
       $role = \Role::update($role, $request);
       $data = [];
       if ($role) {
           $data['successful'] = true;
           $data['message'] = 'Record Update Successfully';
           $data['role_id'] = $role;
       }else{
           $data['successful'] = false;
           $data['message'] = 'Record Not Update Successfully';
       }
       return response()->json($data);
    }

    /*
    * Delete $role
    * @return  void
    */ 
    public function destroy($role)
     {
       //Delete roles
       $role = \Role::destroy($role);
       $data = [];
       if ($role) {
           $data['successful'] = true;
           $data['message'] = 'Record Delete Successfully';

       }else{
           $data['successful'] = false;
           $data['message'] = 'Record Not Delete Successfully';
       }
       return response()->json($data);
    }
}