<?php

namespace App\Http\Controllers; 
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class CountryController extends Controller 
{
    
    /**
     * Display a listing of the resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(\Country::all());
    }

    /**
     * Display the specified resource.
     *
     * @param    \App\Models\Country  $Country
     * @return  \Illuminate\Http\Response
     */
    public function show($id)
    {
        $country = \Country::find($id);
        return response()->json($country);
    } 
 
    /*
    * Store Country
    * @return  void
    */    
    public function store(Request $request)
     {
       //Save countries
       $country = \Country::store($request);
       $data = [];
       if ($country) {
           $data['successful'] = true;
           $data['message'] = 'Record Entered Successfully';
           $data['last_insert_id'] = $country->id;
       }else{
           $data['successful'] = false;
           $data['message'] = 'Record Not Entered Successfully';
       }
       return response()->json($data);
  }

    /*
    * Update Country
    * @return  void
    */ 
    public function update($country,Request $request)
     {
       //Update countries
       $country = \Country::update($country, $request);
       $data = [];
       if ($country) {
           $data['successful'] = true;
           $data['message'] = 'Record Update Successfully';
           $data['country_id'] = $country;
       }else{
           $data['successful'] = false;
           $data['message'] = 'Record Not Update Successfully';
       }
       return response()->json($data);
    }

    /*
    * Delete $country
    * @return  void
    */ 
    public function destroy($country)
     {
       //Delete countries
       $country = \Country::destroy($country);
       $data = [];
       if ($country) {
           $data['successful'] = true;
           $data['message'] = 'Record Delete Successfully';

       }else{
           $data['successful'] = false;
           $data['message'] = 'Record Not Delete Successfully';
       }
       return response()->json($data);
    }
}