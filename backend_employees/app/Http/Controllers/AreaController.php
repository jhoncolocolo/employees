<?php

namespace App\Http\Controllers; 
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class AreaController extends Controller 
{
    
    /**
     * Display a listing of the resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(\Area::all());
    }

    /**
     * Display the specified resource.
     *
     * @param    \App\Models\Area  $Area
     * @return  \Illuminate\Http\Response
     */
    public function show($id)
    {
        $area = \Area::find($id);
        return response()->json($area);
    } 
 
    /*
    * Store Area
    * @return  void
    */    
    public function store(Request $request)
     {
       //Save areas
       $area = \Area::store($request);
       $data = [];
       if ($area) {
           $data['successful'] = true;
           $data['message'] = 'Record Entered Successfully';
           $data['last_insert_id'] = $area->id;
       }else{
           $data['successful'] = false;
           $data['message'] = 'Record Not Entered Successfully';
       }
       return response()->json($data);
  }

    /*
    * Update Area
    * @return  void
    */ 
    public function update($area,Request $request)
     {
       //Update areas
       $area = \Area::update($area, $request);
       $data = [];
       if ($area) {
           $data['successful'] = true;
           $data['message'] = 'Record Update Successfully';
           $data['area_id'] = $area;
       }else{
           $data['successful'] = false;
           $data['message'] = 'Record Not Update Successfully';
       }
       return response()->json($data);
    }

    /*
    * Delete $area
    * @return  void
    */ 
    public function destroy($area)
     {
       //Delete areas
       $area = \Area::destroy($area);
       $data = [];
       if ($area) {
           $data['successful'] = true;
           $data['message'] = 'Record Delete Successfully';

       }else{
           $data['successful'] = false;
           $data['message'] = 'Record Not Delete Successfully';
       }
       return response()->json($data);
    }
}