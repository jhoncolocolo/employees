<?php

namespace App\Http\Controllers; 
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class IdentificationTypeController extends Controller 
{
    
    /**
     * Display a listing of the resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(\IdentificationType::all());
    }

    /**
     * Display the specified resource.
     *
     * @param    \App\Models\IdentificationType  $IdentificationType
     * @return  \Illuminate\Http\Response
     */
    public function show($id)
    {
        $identificationType = \IdentificationType::find($id);
        return response()->json($identificationType);
    } 
 
    /*
    * Store IdentificationType
    * @return  void
    */    
    public function store(Request $request)
     {
       //Save identificationTypes
       $identificationType = \IdentificationType::store($request);
       $data = [];
       if ($identificationType) {
           $data['successful'] = true;
           $data['message'] = 'Record Entered Successfully';
           $data['last_insert_id'] = $identificationType->id;
       }else{
           $data['successful'] = false;
           $data['message'] = 'Record Not Entered Successfully';
       }
       return response()->json($data);
  }

    /*
    * Update IdentificationType
    * @return  void
    */ 
    public function update($identificationType,Request $request)
     {
       //Update identificationTypes
       $identificationType = \IdentificationType::update($identificationType, $request);
       $data = [];
       if ($identificationType) {
           $data['successful'] = true;
           $data['message'] = 'Record Update Successfully';
           $data['identification_type_id'] = $identificationType;
       }else{
           $data['successful'] = false;
           $data['message'] = 'Record Not Update Successfully';
       }
       return response()->json($data);
    }

    /*
    * Delete $identificationType
    * @return  void
    */ 
    public function destroy($identificationType)
     {
       //Delete identificationTypes
       $identificationType = \IdentificationType::destroy($identificationType);
       $data = [];
       if ($identificationType) {
           $data['successful'] = true;
           $data['message'] = 'Record Delete Successfully';

       }else{
           $data['successful'] = false;
           $data['message'] = 'Record Not Delete Successfully';
       }
       return response()->json($data);
    }
}