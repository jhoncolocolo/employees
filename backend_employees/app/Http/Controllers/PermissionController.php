<?php

namespace App\Http\Controllers; 
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PermissionController extends Controller 
{
    
    /**
     * Display a listing of the resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(\Permission::all());
    }

    /**
     * Display the specified resource.
     *
     * @param    \App\Models\Permission  $Permission
     * @return  \Illuminate\Http\Response
     */
    public function show($id)
    {
        $permission = \Permission::find($id);
        return response()->json($permission);
    } 
 
    /*
    * Store Permission
    * @return  void
    */    
    public function store(Request $request)
     {
       //Save permissions
       $permission = \Permission::store($request);
       $data = [];
       if ($permission) {
           $data['successful'] = true;
           $data['message'] = 'Record Entered Successfully';
           $data['last_insert_id'] = $permission->id;
       }else{
           $data['successful'] = false;
           $data['message'] = 'Record Not Entered Successfully';
       }
       return response()->json($data);
  }

    /*
    * Update Permission
    * @return  void
    */ 
    public function update($permission,Request $request)
     {
       //Update permissions
       $permission = \Permission::update($permission, $request);
       $data = [];
       if ($permission) {
           $data['successful'] = true;
           $data['message'] = 'Record Update Successfully';
           $data['permission_id'] = $permission;
       }else{
           $data['successful'] = false;
           $data['message'] = 'Record Not Update Successfully';
       }
       return response()->json($data);
    }

    /*
    * Delete $permission
    * @return  void
    */ 
    public function destroy($permission)
     {
       //Delete permissions
       $permission = \Permission::destroy($permission);
       $data = [];
       if ($permission) {
           $data['successful'] = true;
           $data['message'] = 'Record Delete Successfully';

       }else{
           $data['successful'] = false;
           $data['message'] = 'Record Not Delete Successfully';
       }
       return response()->json($data);
    }
}