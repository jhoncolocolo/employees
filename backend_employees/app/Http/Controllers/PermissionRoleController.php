<?php

namespace App\Http\Controllers; 
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PermissionRoleController extends Controller 
{
    
    /**
     * Display a listing of the resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(\PermissionRole::all());
    }

    /**
     * Display the specified resource.
     *
     * @param    \App\Models\PermissionRole  $PermissionRole
     * @return  \Illuminate\Http\Response
     */
    public function show($id)
    {
        $permissionRole = \PermissionRole::find($id);
        return response()->json($permissionRole);
    } 
 
    /*
    * Store PermissionRole
    * @return  void
    */    
    public function store(Request $request)
     {
       //Save permissionRoles
       $permissionRole = \PermissionRole::store($request);
       $data = [];
       if ($permissionRole) {
           $data['successful'] = true;
           $data['message'] = 'Record Entered Successfully';
           $data['last_insert_id'] = $permissionRole->id;
       }else{
           $data['successful'] = false;
           $data['message'] = 'Record Not Entered Successfully';
       }
       return response()->json($data);
  }

    /*
    * Update PermissionRole
    * @return  void
    */ 
    public function update($permissionRole,Request $request)
     {
       //Update permissionRoles
       $permissionRole = \PermissionRole::update($permissionRole, $request);
       $data = [];
       if ($permissionRole) {
           $data['successful'] = true;
           $data['message'] = 'Record Update Successfully';
           $data['permission_role_id'] = $permissionRole;
       }else{
           $data['successful'] = false;
           $data['message'] = 'Record Not Update Successfully';
       }
       return response()->json($data);
    }

    /*
    * Delete $permissionRole
    * @return  void
    */ 
    public function destroy($permissionRole)
     {
       //Delete permissionRoles
       $permissionRole = \PermissionRole::destroy($permissionRole);
       $data = [];
       if ($permissionRole) {
           $data['successful'] = true;
           $data['message'] = 'Record Delete Successfully';

       }else{
           $data['successful'] = false;
           $data['message'] = 'Record Not Delete Successfully';
       }
       return response()->json($data);
    }
}