<?php

namespace App\Http\Controllers; 
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Requests\RequestEmployee;
use App\Http\Requests\RequestEmployeeUpdate;

class UserController extends Controller 
{
    
    /**
     * Display a listing of the resource.
     *
     * @return  \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $typeParam = $request->get('typeParam');
        $value = $request->get('value');
        return response()->json(\User::allDetails($typeParam,$value));
    }

    /**
     * Display the specified resource.
     *
     * @param    \App\Models\User  $User
     * @return  \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = \User::find($id);
        return response()->json($user);
    } 
 
    /*
    * Store User
    * @return  void
    */    
    public function store(RequestEmployee $request)
     {
       //Save users
       $user = \User::store($request);
       $data = [];
       if ($user) {
           $data['successful'] = true;
           $data['message'] = 'Record Entered Successfully';
           $data['last_insert_id'] = $user->id;
       }else{
           $data['successful'] = false;
           $data['message'] = 'Record Not Entered Successfully';
       }
       return response()->json($data);
  }

    /*
    * Update User
    * @return  void
    */ 
    public function update($user,RequestEmployeeUpdate $request)
     {
       //Update users
       $user = \User::update($user, $request);
       $data = [];
       if ($user) {
           $data['successful'] = true;
           $data['message'] = 'Record Update Successfully';
           $data['user_id'] = $user;
       }else{
           $data['successful'] = false;
           $data['message'] = 'Record Not Update Successfully';
       }
       return response()->json($data);
    }

    /*
    * Delete $user
    * @return  void
    */ 
    public function destroy($user)
     {
       //Delete users
       $user = \User::destroy($user);
       $data = [];
       if ($user) {
           $data['successful'] = true;
           $data['message'] = 'Record Delete Successfully';

       }else{
           $data['successful'] = false;
           $data['message'] = 'Record Not Delete Successfully';
       }
       return response()->json($data);
    }
}