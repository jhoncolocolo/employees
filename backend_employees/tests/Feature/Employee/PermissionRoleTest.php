<?php

namespace Tests\Feature\Employee;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\PermissionRole;
use App\Models\Permission;
use App\Models\Role;
use Tests\Feature\Employee\Factory\MasterFactory;

class PermissionRoleTest extends TestCase
{
    use WithFaker;
    use RefreshDatabase;
    protected function setUp(): void
    {
        parent::setUp();
        //Base Factories
        MasterFactory::createBase();
        $this->setUpFaker();
    }
    /**
    * test get all permissionRoles.
    *
    * @return  void
    */
   public function test_permission_roles_get_all()
   {
        PermissionRole::create([
            'permission_id' => $permission_id_1 = Permission::inRandomOrder()->first()->id,
            'role_id' => $role_id_1 = Role::inRandomOrder()->first()->id,
        ]);

        PermissionRole::create([
            'permission_id' => $permission_id_2 = Permission::inRandomOrder()->first()->id,
            'role_id' => $role_id_2 = Role::inRandomOrder()->first()->id,
        ]);

       $response = $this->json('GET','api/permission_roles');

       $response->assertJson([
            [
                'permission_id' => $permission_id_1,
                'role_id' => $role_id_1,
            ],
            [
                'permission_id' => $permission_id_2,
                'role_id' => $role_id_2,
            ]
        ]);

        $this->assertDatabaseCount('permission_roles', 2);

       $response->assertStatus(200);
   }


    /**
    * test get permissionRole By Id.
    *
    * @return  void
    */
   public function test_permission_roles_get_by_id()
   {
       $permissionRole = PermissionRole::create([
            'permission_id' => $permission_id_1 =  Permission::inRandomOrder()->first()->id,
            'role_id' => $role_id_1 =  Role::inRandomOrder()->first()->id,
        ]);

       $response = $this->json('GET','api/permission_roles/'.$permissionRole->id);

       $response->assertJson([
            'permission_id' => $permission_id_1,
            'role_id' => $role_id_1,
        ]);

       $response->assertStatus(200);
   }

   /**
     * test create permissionRole.
     *
     * @return  void
     */
    public function test_permission_roles_create()
    {

        $response = $this->json('POST','api/permission_roles',[
            'permission_id' => Permission::inRandomOrder()->first()->id,
            'role_id' => Role::inRandomOrder()->first()->id,
        ]);
        $this->assertDatabaseCount('permission_roles', 1);
        $response->assertStatus(200);
    }

    /**
     * test update permissionRole.
     *
     * @return  void
     */
    public function test_permission_roles_update()
    {
        $permissionRole = PermissionRole::create([
            'permission_id' => Permission::inRandomOrder()->first()->id,
            'role_id' => Role::inRandomOrder()->first()->id,
        ]);
        $response = $this->json('PUT','api/permission_roles/'.$permissionRole->id,[
            'permission_id' => $permission_id = Permission::inRandomOrder()->first()->id,
          'role_id' => $permissionRole->role_id,
          
        ]);

        $this->assertDatabaseHas('permission_roles', [
            'permission_id' => $permission_id,        
        ]);

        $response->assertStatus(200);
    }

    /**
     * test delete permissionRole.
     *
     * @return  void
     */
    public function test_permission_roles_delete()
    {
        $permissionRole = PermissionRole::create([
            'permission_id' => Permission::inRandomOrder()->first()->id,
            'role_id' => Role::inRandomOrder()->first()->id,
        ]);

        $response = $this->json('DELETE','api/permission_roles/'.$permissionRole->id);

        $this->assertDatabaseCount('permission_roles', 0);

        $response->assertStatus(200);
    }
}