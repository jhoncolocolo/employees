<?php

namespace Tests\Feature\Employee;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\IdentificationType;

class IdentificationTypeTest extends TestCase
{
    use WithFaker;
    use RefreshDatabase;
    /**
    * test get all identificationTypes.
    *
    * @return  void
    */
   public function test_identification_types_get_all()
   {
        IdentificationType::create([
            'name' => $name_1 = $this->faker->text(255),
        ]);

        IdentificationType::create([
            'name' => $name_2 = $this->faker->text(255),
        ]);

       $response = $this->json('GET','api/identification_types');

       $response->assertJson([
            [
                'name' => $name_1,
            ],
            [
                'name' => $name_2,
            ]
        ]);

        $this->assertDatabaseCount('identification_types', 2);

       $response->assertStatus(200);
   }


    /**
    * test get identificationType By Id.
    *
    * @return  void
    */
   public function test_identification_types_get_by_id()
   {
       $identificationType = IdentificationType::create([
            'name' => $name_1 =  $this->faker->text(255),
        ]);

       $response = $this->json('GET','api/identification_types/'.$identificationType->id);

       $response->assertJson([
            'name' => $name_1,
        ]);

       $response->assertStatus(200);
   }

   /**
     * test create identificationType.
     *
     * @return  void
     */
    public function test_identification_types_create()
    {

        $response = $this->json('POST','api/identification_types',[
            'name' => $this->faker->text(255),
        ]);
        $this->assertDatabaseCount('identification_types', 1);
        $response->assertStatus(200);
    }

    /**
     * test update identificationType.
     *
     * @return  void
     */
    public function test_identification_types_update()
    {
        $identificationType = IdentificationType::create([
            'name' => $this->faker->text(255),
        ]);
        $response = $this->json('PUT','api/identification_types/'.$identificationType->id,[
            'name' => $name = $this->faker->text(255),
          
        ]);

        $this->assertDatabaseHas('identification_types', [
            'name' => $name,        
        ]);

        $response->assertStatus(200);
    }

    /**
     * test delete identificationType.
     *
     * @return  void
     */
    public function test_identification_types_delete()
    {
        $identificationType = IdentificationType::create([
            'name' => $this->faker->text(255),
        ]);

        $response = $this->json('DELETE','api/identification_types/'.$identificationType->id);

        $this->assertDatabaseCount('identification_types', 0);

        $response->assertStatus(200);
    }
}