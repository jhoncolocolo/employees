<?php

namespace Tests\Feature\Employee;
use Illuminate\Support\Str;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;
use App\Models\Area;
use App\Models\Country;
use App\Models\IdentificationType;
use Tests\Feature\Employee\Factory\MasterFactory;

class UserTest extends TestCase
{
    use WithFaker;
    use RefreshDatabase;
    protected function setUp(): void
    {
        parent::setUp();
        //Base Factories
        MasterFactory::createBase();
        $this->setUpFaker();
    }
    /**
    * test get all users.
    *
    * @return  void
    */
   public function test_users_get_all()
   {
        User::create([
            'area_id' => $area_id_1 = Area::inRandomOrder()->first()->id,
            'country_id' => $country_id_1 = Country::inRandomOrder()->first()->id,
            'identification_type_id' => $identification_type_id_1 = IdentificationType::inRandomOrder()->first()->id,
            'identification_number' => $identification_number_1 = $this->faker->text(20),
            'user_name' => $user_name_1 = $this->faker->text(255),
            'email' => $email_1 = $this->faker->unique()->safeEmail(),
            'password' => $password_1 = '$2y$10$NdKdOBqdcInc/w3Bvu8JPObhxpbiKZxQN0V/2vR6bDFsJwg7LdwCm',
            'first_name' => $first_name_1 = $this->faker->text(20),
            'other_names' => $other_names_1 = $this->faker->text(50),
            'surname' => $surname_1 = $this->faker->text(20),
            'second_surname' => $second_surname_1 = $this->faker->text(20),
            'register' => $register_1 = $this->faker->date($format = 'Y-m-d', $max = 'now'),
            'status' => $status_1 = $this->faker->randomElement(array('0','1')),
            'email_verified_at' => $email_verified_at_1 = $this->faker->dateTime($max = 'now')->format('Y-m-d H:i:s'),
            'remember_token' => $remember_token_1 = Str::random(10),
        ]);

        User::create([
            'area_id' => $area_id_2 = Area::inRandomOrder()->first()->id,
            'country_id' => $country_id_2 = Country::inRandomOrder()->first()->id,
            'identification_type_id' => $identification_type_id_2 = IdentificationType::inRandomOrder()->first()->id,
            'identification_number' => $identification_number_2 = $this->faker->text(20),
            'user_name' => $user_name_2 = $this->faker->text(255),
            'email' => $email_2 = $this->faker->unique()->safeEmail(),
            'password' => $password_2 = '$2y$10$NdKdOBqdcInc/w3Bvu8JPObhxpbiKZxQN0V/2vR6bDFsJwg7LdwCm',
            'first_name' => $first_name_2 = $this->faker->text(20),
            'other_names' => $other_names_2 = $this->faker->text(50),
            'surname' => $surname_2 = $this->faker->text(20),
            'second_surname' => $second_surname_2 = $this->faker->text(20),
            'register' => $register_2 = $this->faker->date($format = 'Y-m-d', $max = 'now'),
            'status' => $status_2 = $this->faker->randomElement(array('0','1')),
            'email_verified_at' => $email_verified_at_2 = $this->faker->dateTime($max = 'now')->format('Y-m-d H:i:s'),
            'remember_token' => $remember_token_2 = Str::random(10),
        ]);

       $response = $this->json('GET','api/users');

       $response->assertJson([
            [
                'area_id' => $area_id_1,
                'country_id' => $country_id_1,
                'identification_type_id' => $identification_type_id_1,
                'identification_number' => $identification_number_1,
                'user_name' => $user_name_1,
                'email' => $email_1,
                'password' => $password_1,
                'first_name' => $first_name_1,
                'other_names' => $other_names_1,
                'surname' => $surname_1,
                'second_surname' => $second_surname_1,
                'register' => $register_1,
                'status' => $status_1,
                'email_verified_at' => $email_verified_at_1,
                'remember_token' => $remember_token_1,
            ],
            [
                'area_id' => $area_id_2,
                'country_id' => $country_id_2,
                'identification_type_id' => $identification_type_id_2,
                'identification_number' => $identification_number_2,
                'user_name' => $user_name_2,
                'email' => $email_2,
                'password' => $password_2,
                'first_name' => $first_name_2,
                'other_names' => $other_names_2,
                'surname' => $surname_2,
                'second_surname' => $second_surname_2,
                'register' => $register_2,
                'status' => $status_2,
                'email_verified_at' => $email_verified_at_2,
                'remember_token' => $remember_token_2,
            ]
        ]);

        $this->assertDatabaseCount('users', 2);

       $response->assertStatus(200);
   }


    /**
    * test get user By Id.
    *
    * @return  void
    */
   public function test_users_get_by_id()
   {
       $user = User::create([
            'area_id' => $area_id_1 =  Area::inRandomOrder()->first()->id,
            'country_id' => $country_id_1 =  Country::inRandomOrder()->first()->id,
            'identification_type_id' => $identification_type_id_1 =  IdentificationType::inRandomOrder()->first()->id,
            'identification_number' => $identification_number_1 =  $this->faker->text(20),
            'user_name' => $user_name_1 =  $this->faker->text(255),
            'email' => $email_1 =  $this->faker->unique()->safeEmail(),
            'password' => $password_1 =  '$2y$10$NdKdOBqdcInc/w3Bvu8JPObhxpbiKZxQN0V/2vR6bDFsJwg7LdwCm',
            'first_name' => $first_name_1 =  $this->faker->text(20),
            'other_names' => $other_names_1 =  $this->faker->text(50),
            'surname' => $surname_1 =  $this->faker->text(20),
            'second_surname' => $second_surname_1 =  $this->faker->text(20),
            'register' => $register_1 =  $this->faker->date($format = 'Y-m-d', $max = 'now'),
            'status' => $status_1 =  $this->faker->randomElement(array('0','1')),
            'email_verified_at' => $email_verified_at_1 =  $this->faker->dateTime($max = 'now')->format('Y-m-d H:i:s'),
            'remember_token' => $remember_token_1 =  Str::random(10),
        ]);

       $response = $this->json('GET','api/users/'.$user->id);

       $response->assertJson([
            'area_id' => $area_id_1,
            'country_id' => $country_id_1,
            'identification_type_id' => $identification_type_id_1,
            'identification_number' => $identification_number_1,
            'user_name' => $user_name_1,
            'email' => $email_1,
            'password' => $password_1,
            'first_name' => $first_name_1,
            'other_names' => $other_names_1,
            'surname' => $surname_1,
            'second_surname' => $second_surname_1,
            'register' => $register_1,
            'status' => $status_1,
            'email_verified_at' => $email_verified_at_1,
            'remember_token' => $remember_token_1,
        ]);

       $response->assertStatus(200);
   }

   /**
     * test create user.
     *
     * @return  void
     */
    public function test_users_create()
    {

        $response = $this->json('POST','api/users',[
            'area_id' => Area::inRandomOrder()->first()->id,
            'country_id' => Country::inRandomOrder()->first()->id,
            'identification_type_id' => IdentificationType::inRandomOrder()->first()->id,
            'identification_number' => $this->faker->text(20),
            'user_name' => $this->faker->text(255),
            'email' => $this->faker->unique()->safeEmail(),
            'password' => '$2y$10$NdKdOBqdcInc/w3Bvu8JPObhxpbiKZxQN0V/2vR6bDFsJwg7LdwCm',
            'first_name' => $this->faker->text(20),
            'other_names' => $this->faker->text(50),
            'surname' => $this->faker->text(20),
            'second_surname' => $this->faker->text(20),
            'register' => $this->faker->date($format = 'Y-m-d', $max = 'now'),
            'status' => $this->faker->randomElement(array('0','1')),
            'email_verified_at' => $this->faker->dateTime($max = 'now')->format('Y-m-d H:i:s'),
            'remember_token' => Str::random(10),
        ]);
        $this->assertDatabaseCount('users', 1);
        $response->assertStatus(200);
    }

    /**
     * test update user.
     *
     * @return  void
     */
    public function test_users_update()
    {
        $user = User::create([
            'area_id' => Area::inRandomOrder()->first()->id,
            'country_id' => Country::inRandomOrder()->first()->id,
            'identification_type_id' => IdentificationType::inRandomOrder()->first()->id,
            'identification_number' => $this->faker->text(20),
            'user_name' => $this->faker->text(255),
            'email' => $this->faker->unique()->safeEmail(),
            'password' => '$2y$10$NdKdOBqdcInc/w3Bvu8JPObhxpbiKZxQN0V/2vR6bDFsJwg7LdwCm',
            'first_name' => $this->faker->text(20),
            'other_names' => $this->faker->text(50),
            'surname' => $this->faker->text(20),
            'second_surname' => $this->faker->text(20),
            'register' => $this->faker->date($format = 'Y-m-d', $max = 'now'),
            'status' => $this->faker->randomElement(array('0','1')),
            'email_verified_at' => $this->faker->dateTime($max = 'now')->format('Y-m-d H:i:s'),
            'remember_token' => Str::random(10),
        ]);
        $response = $this->json('PUT','api/users/'.$user->id,[
            'area_id' => $area_id = Area::inRandomOrder()->first()->id,
          'country_id' => $user->country_id,
          'identification_type_id' => $user->identification_type_id,
          'identification_number' => $user->identification_number,
          'user_name' => $user->user_name,
          'email' => $user->email,
          'password' => $user->password,
          'first_name' => $user->first_name,
          'other_names' => $user->other_names,
          'surname' => $user->surname,
          'second_surname' => $user->second_surname,
          'register' => $user->register,
          'status' => $user->status,
          'email_verified_at' => $user->email_verified_at,
          'remember_token' => $user->remember_token,
          
        ]);

        $this->assertDatabaseHas('users', [
            'area_id' => $area_id,        
        ]);

        $response->assertStatus(200);
    }

    /**
     * test delete user.
     *
     * @return  void
     */
    public function test_users_delete()
    {
        $user = User::create([
            'area_id' => Area::inRandomOrder()->first()->id,
            'country_id' => Country::inRandomOrder()->first()->id,
            'identification_type_id' => IdentificationType::inRandomOrder()->first()->id,
            'identification_number' => $this->faker->text(20),
            'user_name' => $this->faker->text(255),
            'email' => $this->faker->unique()->safeEmail(),
            'password' => '$2y$10$NdKdOBqdcInc/w3Bvu8JPObhxpbiKZxQN0V/2vR6bDFsJwg7LdwCm',
            'first_name' => $this->faker->text(20),
            'other_names' => $this->faker->text(50),
            'surname' => $this->faker->text(20),
            'second_surname' => $this->faker->text(20),
            'register' => $this->faker->date($format = 'Y-m-d', $max = 'now'),
            'status' => $this->faker->randomElement(array('0','1')),
            'email_verified_at' => $this->faker->dateTime($max = 'now')->format('Y-m-d H:i:s'),
            'remember_token' => Str::random(10),
        ]);

        $response = $this->json('DELETE','api/users/'.$user->id);

        $this->assertDatabaseCount('users', 0);

        $response->assertStatus(200);
    }
}