<?php

namespace Tests\Feature\Employee;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Country;

class CountryTest extends TestCase
{
    use WithFaker;
    use RefreshDatabase;
    /**
    * test get all countries.
    *
    * @return  void
    */
   public function test_countries_get_all()
   {
        Country::create([
            'name' => $name_1 = $this->faker->text(255),
        ]);

        Country::create([
            'name' => $name_2 = $this->faker->text(255),
        ]);

       $response = $this->json('GET','api/countries');

       $response->assertJson([
            [
                'name' => $name_1,
            ],
            [
                'name' => $name_2,
            ]
        ]);

        $this->assertDatabaseCount('countries', 2);

       $response->assertStatus(200);
   }


    /**
    * test get country By Id.
    *
    * @return  void
    */
   public function test_countries_get_by_id()
   {
       $country = Country::create([
            'name' => $name_1 =  $this->faker->text(255),
        ]);

       $response = $this->json('GET','api/countries/'.$country->id);

       $response->assertJson([
            'name' => $name_1,
        ]);

       $response->assertStatus(200);
   }

   /**
     * test create country.
     *
     * @return  void
     */
    public function test_countries_create()
    {

        $response = $this->json('POST','api/countries',[
            'name' => $this->faker->text(255),
        ]);
        $this->assertDatabaseCount('countries', 1);
        $response->assertStatus(200);
    }

    /**
     * test update country.
     *
     * @return  void
     */
    public function test_countries_update()
    {
        $country = Country::create([
            'name' => $this->faker->text(255),
        ]);
        $response = $this->json('PUT','api/countries/'.$country->id,[
            'name' => $name = $this->faker->text(255),
          
        ]);

        $this->assertDatabaseHas('countries', [
            'name' => $name,        
        ]);

        $response->assertStatus(200);
    }

    /**
     * test delete country.
     *
     * @return  void
     */
    public function test_countries_delete()
    {
        $country = Country::create([
            'name' => $this->faker->text(255),
        ]);

        $response = $this->json('DELETE','api/countries/'.$country->id);

        $this->assertDatabaseCount('countries', 0);

        $response->assertStatus(200);
    }
}