<?php

namespace Tests\Feature\Employee\Factory;
use Illuminate\Support\Str;
use Faker\Generator as Faker;
use Tests\TestCase;
use App\Models\Permission;
use App\Models\Role;
use App\Models\IdentificationType;
use App\Models\Country;
use App\Models\Area;

class MasterFactory
{

    public function __construct()
    {
    }

    public static function createBase(): MasterFactory
    {
        $faker = \Faker\Factory::create();
        try {

            //Permissions
            Permission::create([
                'name' => $faker->text(255),
                'route' => $faker->text(255),
                'path' => $faker->text(255),
                'description' => $faker->sentence(),
            ]);


            //Roles
            Role::create([
                'name' => $faker->text(255),
                'role' => $faker->text(120),
                'description' => $faker->sentence(),
            ]);


            //IdentificationTypes
            IdentificationType::create([
                'name' => $faker->text(255),
            ]);


            //Countries
            Country::create([
                'name' => $faker->text(255),
            ]);


            //Areas
            Area::create([
                'name' => $faker->text(255),
            ]);



        } catch (\Illuminate\Contracts\Filesystem\FileNotFoundException $e) {
            echo $e;
        }

        return new MasterFactory();
    }
}