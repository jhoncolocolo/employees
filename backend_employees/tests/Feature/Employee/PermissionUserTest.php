<?php

namespace Tests\Feature\Employee;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\PermissionUser;
use App\Models\Permission;
use App\Models\User;
use Tests\Feature\Employee\Factory\MasterFactory;
use Tests\Feature\Employee\Factory\TablesDependentFactory;

class PermissionUserTest extends TestCase
{
    use WithFaker;
    use RefreshDatabase;
    protected function setUp(): void
    {
        parent::setUp();
        //Base Factories
        MasterFactory::createBase();
        $this->setUpFaker();
        TablesDependentFactory::createTables('users');
        
    }
    /**
    * test get all permissionUsers.
    *
    * @return  void
    */
   public function test_permission_users_get_all()
   {
        PermissionUser::create([
            'permission_id' => $permission_id_1 = Permission::inRandomOrder()->first()->id,
            'user_id' => $user_id_1 = User::inRandomOrder()->first()->id,
        ]);

        PermissionUser::create([
            'permission_id' => $permission_id_2 = Permission::inRandomOrder()->first()->id,
            'user_id' => $user_id_2 = User::inRandomOrder()->first()->id,
        ]);

       $response = $this->json('GET','api/permission_users');

       $response->assertJson([
            [
                'permission_id' => $permission_id_1,
                'user_id' => $user_id_1,
            ],
            [
                'permission_id' => $permission_id_2,
                'user_id' => $user_id_2,
            ]
        ]);

        $this->assertDatabaseCount('permission_users', 2);

       $response->assertStatus(200);
   }


    /**
    * test get permissionUser By Id.
    *
    * @return  void
    */
   public function test_permission_users_get_by_id()
   {
       $permissionUser = PermissionUser::create([
            'permission_id' => $permission_id_1 =  Permission::inRandomOrder()->first()->id,
            'user_id' => $user_id_1 =  User::inRandomOrder()->first()->id,
        ]);

       $response = $this->json('GET','api/permission_users/'.$permissionUser->id);

       $response->assertJson([
            'permission_id' => $permission_id_1,
            'user_id' => $user_id_1,
        ]);

       $response->assertStatus(200);
   }

   /**
     * test create permissionUser.
     *
     * @return  void
     */
    public function test_permission_users_create()
    {

        $response = $this->json('POST','api/permission_users',[
            'permission_id' => Permission::inRandomOrder()->first()->id,
            'user_id' => User::inRandomOrder()->first()->id,
        ]);
        $this->assertDatabaseCount('permission_users', 1);
        $response->assertStatus(200);
    }

    /**
     * test update permissionUser.
     *
     * @return  void
     */
    public function test_permission_users_update()
    {
        $permissionUser = PermissionUser::create([
            'permission_id' => Permission::inRandomOrder()->first()->id,
            'user_id' => User::inRandomOrder()->first()->id,
        ]);
        $response = $this->json('PUT','api/permission_users/'.$permissionUser->id,[
            'permission_id' => $permission_id = Permission::inRandomOrder()->first()->id,
          'user_id' => $permissionUser->user_id,
          
        ]);

        $this->assertDatabaseHas('permission_users', [
            'permission_id' => $permission_id,        
        ]);

        $response->assertStatus(200);
    }

    /**
     * test delete permissionUser.
     *
     * @return  void
     */
    public function test_permission_users_delete()
    {
        $permissionUser = PermissionUser::create([
            'permission_id' => Permission::inRandomOrder()->first()->id,
            'user_id' => User::inRandomOrder()->first()->id,
        ]);

        $response = $this->json('DELETE','api/permission_users/'.$permissionUser->id);

        $this->assertDatabaseCount('permission_users', 0);

        $response->assertStatus(200);
    }
}