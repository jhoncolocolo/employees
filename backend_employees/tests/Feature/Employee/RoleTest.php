<?php

namespace Tests\Feature\Employee;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Role;

class RoleTest extends TestCase
{
    use WithFaker;
    use RefreshDatabase;
    /**
    * test get all roles.
    *
    * @return  void
    */
   public function test_roles_get_all()
   {
        Role::create([
            'name' => $name_1 = $this->faker->text(255),
            'role' => $role_1 = $this->faker->text(120),
            'description' => $description_1 = $this->faker->sentence(),
        ]);

        Role::create([
            'name' => $name_2 = $this->faker->text(255),
            'role' => $role_2 = $this->faker->text(120),
            'description' => $description_2 = $this->faker->sentence(),
        ]);

       $response = $this->json('GET','api/roles');

       $response->assertJson([
            [
                'name' => $name_1,
                'role' => $role_1,
                'description' => $description_1,
            ],
            [
                'name' => $name_2,
                'role' => $role_2,
                'description' => $description_2,
            ]
        ]);

        $this->assertDatabaseCount('roles', 2);

       $response->assertStatus(200);
   }


    /**
    * test get role By Id.
    *
    * @return  void
    */
   public function test_roles_get_by_id()
   {
       $role = Role::create([
            'name' => $name_1 =  $this->faker->text(255),
            'role' => $role_1 =  $this->faker->text(120),
            'description' => $description_1 =  $this->faker->sentence(),
        ]);

       $response = $this->json('GET','api/roles/'.$role->id);

       $response->assertJson([
            'name' => $name_1,
            'role' => $role_1,
            'description' => $description_1,
        ]);

       $response->assertStatus(200);
   }

   /**
     * test create role.
     *
     * @return  void
     */
    public function test_roles_create()
    {

        $response = $this->json('POST','api/roles',[
            'name' => $this->faker->text(255),
            'role' => $this->faker->text(120),
            'description' => $this->faker->sentence(),
        ]);
        $this->assertDatabaseCount('roles', 1);
        $response->assertStatus(200);
    }

    /**
     * test update role.
     *
     * @return  void
     */
    public function test_roles_update()
    {
        $role = Role::create([
            'name' => $this->faker->text(255),
            'role' => $this->faker->text(120),
            'description' => $this->faker->sentence(),
        ]);
        $response = $this->json('PUT','api/roles/'.$role->id,[
            'name' => $name = $this->faker->text(255),
          'role' => $role->role,
          'description' => $role->description,
          
        ]);

        $this->assertDatabaseHas('roles', [
            'name' => $name,        
        ]);

        $response->assertStatus(200);
    }

    /**
     * test delete role.
     *
     * @return  void
     */
    public function test_roles_delete()
    {
        $role = Role::create([
            'name' => $this->faker->text(255),
            'role' => $this->faker->text(120),
            'description' => $this->faker->sentence(),
        ]);

        $response = $this->json('DELETE','api/roles/'.$role->id);

        $this->assertDatabaseCount('roles', 0);

        $response->assertStatus(200);
    }
}