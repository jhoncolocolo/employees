<?php

namespace Tests\Feature\Employee;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Permission;

class PermissionTest extends TestCase
{
    use WithFaker;
    use RefreshDatabase;
    /**
    * test get all permissions.
    *
    * @return  void
    */
   public function test_permissions_get_all()
   {
        Permission::create([
            'name' => $name_1 = $this->faker->text(255),
            'route' => $route_1 = $this->faker->text(255),
            'path' => $path_1 = $this->faker->text(255),
            'description' => $description_1 = $this->faker->sentence(),
        ]);

        Permission::create([
            'name' => $name_2 = $this->faker->text(255),
            'route' => $route_2 = $this->faker->text(255),
            'path' => $path_2 = $this->faker->text(255),
            'description' => $description_2 = $this->faker->sentence(),
        ]);

       $response = $this->json('GET','api/permissions');

       $response->assertJson([
            [
                'name' => $name_1,
                'route' => $route_1,
                'path' => $path_1,
                'description' => $description_1,
            ],
            [
                'name' => $name_2,
                'route' => $route_2,
                'path' => $path_2,
                'description' => $description_2,
            ]
        ]);

        $this->assertDatabaseCount('permissions', 2);

       $response->assertStatus(200);
   }


    /**
    * test get permission By Id.
    *
    * @return  void
    */
   public function test_permissions_get_by_id()
   {
       $permission = Permission::create([
            'name' => $name_1 =  $this->faker->text(255),
            'route' => $route_1 =  $this->faker->text(255),
            'path' => $path_1 =  $this->faker->text(255),
            'description' => $description_1 =  $this->faker->sentence(),
        ]);

       $response = $this->json('GET','api/permissions/'.$permission->id);

       $response->assertJson([
            'name' => $name_1,
            'route' => $route_1,
            'path' => $path_1,
            'description' => $description_1,
        ]);

       $response->assertStatus(200);
   }

   /**
     * test create permission.
     *
     * @return  void
     */
    public function test_permissions_create()
    {

        $response = $this->json('POST','api/permissions',[
            'name' => $this->faker->text(255),
            'route' => $this->faker->text(255),
            'path' => $this->faker->text(255),
            'description' => $this->faker->sentence(),
        ]);
        $this->assertDatabaseCount('permissions', 1);
        $response->assertStatus(200);
    }

    /**
     * test update permission.
     *
     * @return  void
     */
    public function test_permissions_update()
    {
        $permission = Permission::create([
            'name' => $this->faker->text(255),
            'route' => $this->faker->text(255),
            'path' => $this->faker->text(255),
            'description' => $this->faker->sentence(),
        ]);
        $response = $this->json('PUT','api/permissions/'.$permission->id,[
            'name' => $name = $this->faker->text(255),
          'route' => $permission->route,
          'path' => $permission->path,
          'description' => $permission->description,
          
        ]);

        $this->assertDatabaseHas('permissions', [
            'name' => $name,        
        ]);

        $response->assertStatus(200);
    }

    /**
     * test delete permission.
     *
     * @return  void
     */
    public function test_permissions_delete()
    {
        $permission = Permission::create([
            'name' => $this->faker->text(255),
            'route' => $this->faker->text(255),
            'path' => $this->faker->text(255),
            'description' => $this->faker->sentence(),
        ]);

        $response = $this->json('DELETE','api/permissions/'.$permission->id);

        $this->assertDatabaseCount('permissions', 0);

        $response->assertStatus(200);
    }
}