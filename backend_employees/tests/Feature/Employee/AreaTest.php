<?php

namespace Tests\Feature\Employee;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Area;

class AreaTest extends TestCase
{
    use WithFaker;
    use RefreshDatabase;
    /**
    * test get all areas.
    *
    * @return  void
    */
   public function test_areas_get_all()
   {
        Area::create([
            'name' => $name_1 = $this->faker->text(255),
        ]);

        Area::create([
            'name' => $name_2 = $this->faker->text(255),
        ]);

       $response = $this->json('GET','api/areas');

       $response->assertJson([
            [
                'name' => $name_1,
            ],
            [
                'name' => $name_2,
            ]
        ]);

        $this->assertDatabaseCount('areas', 2);

       $response->assertStatus(200);
   }


    /**
    * test get area By Id.
    *
    * @return  void
    */
   public function test_areas_get_by_id()
   {
       $area = Area::create([
            'name' => $name_1 =  $this->faker->text(255),
        ]);

       $response = $this->json('GET','api/areas/'.$area->id);

       $response->assertJson([
            'name' => $name_1,
        ]);

       $response->assertStatus(200);
   }

   /**
     * test create area.
     *
     * @return  void
     */
    public function test_areas_create()
    {

        $response = $this->json('POST','api/areas',[
            'name' => $this->faker->text(255),
        ]);
        $this->assertDatabaseCount('areas', 1);
        $response->assertStatus(200);
    }

    /**
     * test update area.
     *
     * @return  void
     */
    public function test_areas_update()
    {
        $area = Area::create([
            'name' => $this->faker->text(255),
        ]);
        $response = $this->json('PUT','api/areas/'.$area->id,[
            'name' => $name = $this->faker->text(255),
          
        ]);

        $this->assertDatabaseHas('areas', [
            'name' => $name,        
        ]);

        $response->assertStatus(200);
    }

    /**
     * test delete area.
     *
     * @return  void
     */
    public function test_areas_delete()
    {
        $area = Area::create([
            'name' => $this->faker->text(255),
        ]);

        $response = $this->json('DELETE','api/areas/'.$area->id);

        $this->assertDatabaseCount('areas', 0);

        $response->assertStatus(200);
    }
}