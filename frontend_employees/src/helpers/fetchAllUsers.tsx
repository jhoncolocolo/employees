import {HTTP} from '../resources/resources'
import { UserResponseData } from '../Interfaces/UserResponse';
export const fetchAllUsers = async(id:number=1,type:string=null,value:string=null) : Promise<UserResponseData> =>{
    const resp = await HTTP.get<UserResponseData>(`/users?page=${id}&typeParam=${type}&value=${value}` );
    return resp.data;
}   