import Model from './Model';

class LanguageModel extends Model {

    constructor() {
        super();
        this.count = 0;
    }
}

export default LanguageModel;