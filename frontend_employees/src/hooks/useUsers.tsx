import { useEffect,useState } from 'react';
import { fetchAllUsers } from '../helpers/fetchAllUsers';
import { UserResponseData } from '../Interfaces/UserResponse';
export const useUsers = (page:number=1) =>{
    console.log(page);
	const [isLoading, setIsLoading]= useState(true);
    const [users,setUsers] = useState<UserResponseData>()
    
    useEffect(() =>{
        //load Users
        fetchAllUsers()
        .then(users =>{
            setIsLoading(false);
            setUsers(users);
            
        });
    },[])
    
    return {
        isLoading,
        users
    }
}