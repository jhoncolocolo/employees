import React, { useEffect,useState } from 'react';
import './App.css';
import Navigation from './components/Nav';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import UserIndex from './views/users/UserIndex';
import UserCreate from './views/users/UserCreate';
import UserEdit from './views/users/UserEdit';

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Navigation/>
        <main>
          <Routes>
            <Route path='/users' element={<UserIndex/> }/>
            <Route path='/users/create' element={<UserCreate/> }/>
            <Route path='/users/edit/:id' element={<UserEdit/> }/>
          </Routes>
        </main>
      </BrowserRouter>
    </div>
  );
}

export default App;