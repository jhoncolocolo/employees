import axios from 'axios'
import {useState, useEffect} from 'react'
import {Link} from 'react-router-dom'

const URI = 'http://localhost:4747/identification_types/'


const CompIdentificationTypeLists = () => {
    
    const [identification_types, setIdentificationType] = useState([])
    useEffect( ()=>{
        getIdentificationTypes()
    },[])

    //Procedure for show all IdentificationTypes
    const getIdentificationTypes = async () => {
        const res = await axios.get(URI)
        setIdentificationType(res.data)
    }

    //Procedure for delete IdentificationType 
    const deleteIdentificationType = async (id) => {
       await axios.delete(`${URI}${id}`)
       getIdentificationTypes()
    }

    return(
        <div className='container'>
            <div className='row'>
                <div className='col'>
                    <Link to="/identification_types/create" className='btn btn-primary mt-2 mb-2'><i className="fas fa-plus"></i></Link>
                    <table className='table'>
                        <thead className='table-primary'>
                            <tr>
                                <th>name</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            { identification_types.map ( ( identification_type) => (
                                <tr key={ identification_type.id }>
                                <td>{  identification_type.name }</td>
                                    <td>
                                        <Link to={`/identification_types/edit/${ identification_type.id }`} className='btn btn-info'><i className="fas fa-edit"></i></Link>
                                        <button onClick={ ()=>deleteIdentificationType( identification_type.id ) } className='btn btn-danger'><i className="fas fa-trash-alt"></i></button>
                                    </td>
                                </tr>
                            )) }
                        </tbody>
                    </table>
                </div>    
            </div>
        </div>
    )

}

export default CompIdentificationTypeLists