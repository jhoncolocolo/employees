import axios from 'axios'
import {useState, useEffect} from 'react'
import {Link} from 'react-router-dom'

const URI = 'http://localhost:4747/permissions/'


const CompPermissionLists = () => {
    
    const [permissions, setPermission] = useState([])
    useEffect( ()=>{
        getPermissions()
    },[])

    //Procedure for show all Permissions
    const getPermissions = async () => {
        const res = await axios.get(URI)
        setPermission(res.data)
    }

    //Procedure for delete Permission 
    const deletePermission = async (id) => {
       await axios.delete(`${URI}${id}`)
       getPermissions()
    }

    return(
        <div className='container'>
            <div className='row'>
                <div className='col'>
                    <Link to="/permissions/create" className='btn btn-primary mt-2 mb-2'><i className="fas fa-plus"></i></Link>
                    <table className='table'>
                        <thead className='table-primary'>
                            <tr>
                                <th>name</th>
                                <th>route</th>
                                <th>path</th>
                                <th>description</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            { permissions.map ( ( permission) => (
                                <tr key={ permission.id }>
                                <td>{  permission.name }</td>
                                <td>{  permission.route }</td>
                                <td>{  permission.path }</td>
                                <td>{  permission.description }</td>
                                    <td>
                                        <Link to={`/permissions/edit/${ permission.id }`} className='btn btn-info'><i className="fas fa-edit"></i></Link>
                                        <button onClick={ ()=>deletePermission( permission.id ) } className='btn btn-danger'><i className="fas fa-trash-alt"></i></button>
                                    </td>
                                </tr>
                            )) }
                        </tbody>
                    </table>
                </div>    
            </div>
        </div>
    )

}

export default CompPermissionLists