import axios from 'axios'
import { useState,useEffect } from 'react'
import { useNavigate } from 'react-router-dom'

const URI = 'http://localhost:8000/api/users/'
const URIAreas = 'http://localhost:8000/api/areas/'
const URICountries = 'http://localhost:8000/api/countries/'
const URIIdentificationTypes = 'http://localhost:8000/api/identification_types/'
const UserCreate = () => {

        const [ areas, setAreaData] = useState([])
        const [ countries, setCountryData] = useState([])
        const [ identification_types, setIdentificationTypeData] = useState([])
    
        const [ area_id , setAreaId ] = useState('1')
        const [ country_id , setCountryId ] = useState('1')
        const [ identification_type_id , setIdentificationTypeId ] = useState('1')
        const [ identification_number , setIdentificationNumber ] = useState('')
        const [ first_name , setFirstName ] = useState('')
        const [ other_names , setOtherName ] = useState('')
        const [ surname , setSurname ] = useState('')
        const [ second_surname , setSecondSurname ] = useState('')
        const [ register , setRegister ] = useState('')
        const [ error_id_num , setErrorIdNum ] = useState('')
        const [ error_fir_nam , setErrorFirNam ] = useState('')
        const [ error_oth_nam , setErrorOthNam ] = useState('')
        const [ error_surname , setErrorSurname ] = useState('')
        const [ error_sec_surname , setErrorSecSurname ] = useState('')
        const [ error_register , setErrorRegister ] = useState('')
        const navigate = useNavigate()    
        useEffect( ()=>{
            getAreas()
            getCountries()
            getIdentificationTypes()
        },[])
 
        //Procedure To  Show All Areas
        const getAreas = async () => {
            const res = await axios.get(URIAreas)
            setAreaData(res.data)
        }
        //Procedure To  Show All Countries
        const getCountries = async () => {
            const res = await axios.get(URICountries)
            setCountryData(res.data)
        }
        //Procedure To  Show All IdentificationTypes
        const getIdentificationTypes = async () => {
            const res = await axios.get(URIIdentificationTypes)
            setIdentificationTypeData(res.data)
        }

        const reInitErrors =  () => {
          setErrorFirNam('');
          setErrorOthNam('');
          setErrorSurname('');
          setErrorSecSurname('');
          setErrorRegister('');
          setErrorIdNum('');
        }
    

        //Procedure Store
        const store = async (e) => {
            e.preventDefault();
            await axios.post(URI, {
                                 area_id : area_id,
                                 country_id : country_id,
                                 identification_type_id : identification_type_id,
                                 identification_number : identification_number,
                                 first_name : first_name,
                                 other_names : other_names,
                                 surname : surname,
                                 second_surname : second_surname,
                                 register : register,
                            }).then(response =>{
                                if(response.data.successful){
                                  navigate('/users')
                                }
                            }).catch(function (error) {
                              if (error.response) {
                                // Request made and server responded
                                reInitErrors();
                                const errors = error.response.data.errors;
                                for (const property in errors) {
                                  switch(property) {
                                    case 'first_name':
                                      setErrorFirNam(errors[property]);
                                      break;
                                    case 'other_names':
                                      setErrorOthNam(errors[property]);
                                      break;
                                    case 'surname':
                                      setErrorSurname(errors[property]);
                                      break;
                                    case 'second_surname':
                                      setErrorSecSurname(errors[property]);
                                      break;
                                    case 'register':
                                      setErrorRegister(errors[property]);
                                      break;
                                    case 'identification_number':
                                      setErrorIdNum(errors[property]);
                                      break;
                                  }
                                }
                              }

                            });
        }   

    return (
        <div className="forms">
           <h3>Create User</h3>
            <form  onSubmit={store}>
                <div className="row">
                    <div className="col">
                          <label className="form-label">Area Company</label>
                          <select className="form-control"  value={area_id} onChange={ (e)=> setAreaId(e.target.value)}>
                              {areas.map ( ( area ) => (
                                <option key={ area.id } value={area.id}>{area.name}</option>
                               )) }
                          </select>
                    </div>
                    <div className="col">
                          <label className="form-label">Country</label>
                          <select className="form-control"  value={country_id} onChange={ (e)=> setCountryId(e.target.value)}>
                              {countries.map ( ( country ) => (
                                <option key={ country.id } value={country.id}>{country.name}</option>
                               )) }
                          </select>
                    </div>
                    <div className="col">
                          <label className="form-label">Identification Type</label>
                          <select className="form-control"  value={identification_type_id} onChange={ (e)=> setIdentificationTypeId(e.target.value)}>
                              {identification_types.map ( ( identification_type ) => (
                                <option key={ identification_type.id } value={identification_type.id}>{identification_type.name}</option>
                               )) }
                          </select>
                    </div>
                    <div className="col">
       
                      <label className="form-label">identification_number</label>
                      <input type="text" className="form-control" value={identification_number} onChange={ (e)=> setIdentificationNumber(e.target.value)}/>
                      <span className="form__messages_error">{error_id_num ? error_id_num : ''}</span>
                    </div>
                </div>
                <div className="row">
                    <div className="col">
       
                      <label className="form-label">first_name</label>
                      <input type="text" className="form-control" value={first_name} onChange={ (e)=> setFirstName(e.target.value)}/>
                      <span className="form__messages_error">{error_fir_nam ? error_fir_nam : ''}</span>
                    </div>
                    <div className="col">
       
                      <label className="form-label">other_names</label>
                      <input type="text" className="form-control" value={other_names} onChange={ (e)=> setOtherName(e.target.value)}/>
                      <span className="form__messages_error">{error_oth_nam ? error_oth_nam : ''}</span>
                    </div>
                    <div className="col">
       
                      <label className="form-label">surname</label>
                      <input type="text" className="form-control" value={surname} onChange={ (e)=> setSurname(e.target.value)}/>
                      <span className="form__messages_error">{error_surname ? error_surname : ''}</span>
                    </div>
                    <div className="col">
       
                      <label className="form-label">second_surname</label>
                      <input type="text" className="form-control" value={second_surname} onChange={ (e)=> setSecondSurname(e.target.value)}/>
                      <span className="form__messages_error">{error_sec_surname ? error_sec_surname : ''}</span>
                    </div>
                </div>
                <div className="row">
                    <div className="col">
                      <label className="form-label">Register</label>
                      <input type="date" className="form-control" value={register} onChange={ (e)=> setRegister(e.target.value)} />
                      <span className="form__messages_error">{error_register ? error_register : ''}</span>
                    </div>
                    <div className="col-md-9 form__button">
                      <button type='submit' className='btn btn-primary'>Store</button> 
                    </div>
                      
                </div>
            </form>
        </div>
    )
}

export default UserCreate