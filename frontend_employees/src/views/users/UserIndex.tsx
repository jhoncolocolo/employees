import axios from 'axios';
import { useUsers } from "../../hooks/useUsers";
import Pagination  from "react-js-pagination";
import React, { ChangeEvent } from "react";
import { UserResponseData } from "../../Interfaces/UserResponse";
import { useEffect,useState } from 'react';
import { fetchAllUsers } from "../../helpers/fetchAllUsers";
import { Loading } from "../../components/Loading";
import {Link} from 'react-router-dom'
const URI = 'http://localhost:8000/api/users/'

const UserIndex = () => {
    const [users,setUsers] = useState<UserResponseData>();
    const [isLoading, setIsLoading]= useState(true);
    const [ search, setSearch ] = useState('');
    const [ filter_type_id , setFilterTypeId] = useState('')


    useEffect(() =>{
        //load Users
        getUsers(1);
    },[])

        
    const onSearchChange = ({ target }: ChangeEvent<HTMLInputElement>) => {
        setSearch( target.value );
    }

    //Procedure for show all Languages
    const getUsers = async (id:number) => {
        setIsLoading(true);
        setUsers(null);
        fetchAllUsers(id,filter_type_id,search)
        .then(users =>{
            setIsLoading(false);
            setUsers(users);
        });
    } 

    //Procedure for delete User 
    const deleteUser = async (user) => {
        var result = window.confirm("Está seguro de que desea eliminar el emplado "+user.employee+"?");
        if (result == true) {
            axios.delete(URI+user.id);
            getUsers(1);
        }
    }

    const setNewSearch = () => {
        if(!filter_type_id) console.log("Se debe definir un Filtro");
        if(!search) console.log("Se debe definir un valor para el filtro");
        if(filter_type_id && search){
            getUsers(1);
        }
    }

    const renderUserList = () =>{
        const {data} = users;
        return(
            <React.Fragment>
                    { data.map ( ( user) => (
                        <tr  key={ user.id }>
                            <td>{ user.area_company }</td>
                            <td>{ user.country }</td>
                            <td>{ user.identification_type }</td>
                            <td>{ user.identification }</td>
                            <td>{ user.email}</td>
                            <td>{ user.employee}</td>
                            <td>{ user.register.toString()}</td>
                            <td>{ user.status}</td>
                            <td>
                                <Link to={`/users/edit/${ user.id }`} className='btn btn-info'>&#9997;</Link>
                                <button onClick={ ()=>deleteUser( user ) } className='btn btn-danger'>&#9940;</button>
                            </td>
                        </tr>
                    )) }
            </React.Fragment>
            
        );
    }

    const renderUserLinks= () =>{
        const {current_page,per_page,total} = users;
        return(
            <React.Fragment>
                <div className="mt-3">
                    <Pagination 
                        totalItemsCount={total}
                        activePage={current_page}
                        itemsCountPerPage={per_page}
                        onChange={(pageNumber) => getUsers(pageNumber)}
                        itemClass="page-item"
                        linkClass="page-link"
                        firstPageText="First"
                        lastPageText="Last"
                    />
                </div>
            </React.Fragment>   
        );
    }

    return(
        <div className='container mt-5 container-employee'>
            <div className='row'>
                <div className='col'>
                <div className="Paginator-Bar">
                    <div className="mr-10">
                        <select value={filter_type_id} onChange={ (e)=> setFilterTypeId(e.target.value)} className="form-control">
                            <option value="">Seleccione Su Búsqueda</option>
                            <option value="first_name">Primer Nombre</option>
                            <option value="other_names">Otros Nombres</option>
                            <option value="surname">Primer Apellido</option>
                            <option value="second_surname">Segundo Apellido</option>
                            <option value="identification_type">Tipo de Indentificación</option>
                            <option value="identification">Número de Indentificación</option>
                            <option value="country">País</option>
                            <option value="email">Correo Electrónico</option>
                            <option value="status">Estado</option>
                        </select>
                    </div>
                    <div className="mr-10">
                        <input 
                            type="text"
                            className="mb-2 form-control"
                            placeholder="Buscar Empleado"
                            value={ search }
                            onChange={ onSearchChange }
                        />
                    </div>
                    <div className="mr-10 ml-10 container-button">
                        <button className="button-employee button-employee-cian" onClick={ setNewSearch }>Buscar</button>
                    </div>
                    <div className="mr-10 ml-10 container-button">
                        <Link to="/users/create" className='button-employee button-employee-blue'>Create</Link>
                    </div>
                   </div>
                    <table className='table letter-table'>
                        <thead className='table-primary'>
                            <tr>
                                <th>Area Company</th>
                                <th>Country </th>
                                <th>Identification Type</th>
                                <th>identification_number</th>
                                <th>Email</th>
                                <th>Employee</th>
                                <th>Register</th>
                                <th>Status</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            {users && renderUserList()}
                        </tbody>
                    </table>
                    {users && renderUserLinks()}
                    {
                        isLoading && <Loading />
                    }
                </div>    
            </div>
        </div>
    )

}

export default UserIndex