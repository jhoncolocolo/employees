import axios from 'axios'
import {useState, useEffect} from 'react'
import {Link} from 'react-router-dom'

const URI = 'http://localhost:4747/areas/'


const CompAreaLists = () => {
    
    const [areas, setArea] = useState([])
    useEffect( ()=>{
        getAreas()
    },[])

    //Procedure for show all Areas
    const getAreas = async () => {
        const res = await axios.get(URI)
        setArea(res.data)
    }

    //Procedure for delete Area 
    const deleteArea = async (id) => {
       await axios.delete(`${URI}${id}`)
       getAreas()
    }

    return(
        <div className='container'>
            <div className='row'>
                <div className='col'>
                    <Link to="/areas/create" className='btn btn-primary mt-2 mb-2'><i className="fas fa-plus"></i></Link>
                    <table className='table'>
                        <thead className='table-primary'>
                            <tr>
                                <th>name</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            { areas.map ( ( area) => (
                                <tr key={ area.id }>
                                <td>{  area.name }</td>
                                    <td>
                                        <Link to={`/areas/edit/${ area.id }`} className='btn btn-info'><i className="fas fa-edit"></i></Link>
                                        <button onClick={ ()=>deleteArea( area.id ) } className='btn btn-danger'><i className="fas fa-trash-alt"></i></button>
                                    </td>
                                </tr>
                            )) }
                        </tbody>
                    </table>
                </div>    
            </div>
        </div>
    )

}

export default CompAreaLists