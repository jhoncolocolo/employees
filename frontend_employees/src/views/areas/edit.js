import axios from 'axios'
import { useState,useEffect } from 'react'
import { useNavigate, useParams } from 'react-router-dom'

const URI = 'http://localhost:4747/areas/'
const CompAreaEdit = () => {

    
        const [ name , setName ] = useState('')
        const {id} = useParams()
        const navigate = useNavigate()    
    
        
        useEffect( ()=>{
            getAreaById()
        },[])

    
        const getAreaById = async () => {
            const res = await axios.get(URI+id)
            setName(res.data.name)
        }

        //Procedure to Update
        const update = async (e) => {
            e.preventDefault()
            await axios.put(URI+id, {
                    name : name,
                })
            navigate('/areas')
        }   

    return (
        <div>
            <form  onSubmit={update}>
                <div>
                                <div className="mb-4">
       
                      <label className="form-label">name</label>
                      <input type="text" className="form-control" value={name} onChange={ (e)=> setName(e.target.value)}/>
                    </div>
    
                    <button type='submit' className='btn btn-primary'>Edit</button>   
                </div>
            </form>
        </div>
    )
}

export default CompAreaEdit