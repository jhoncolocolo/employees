import { useState } from 'react'
import { useNavigate } from 'react-router-dom'
import  LanguageModel  from '../../Models/Language'

const CompPermissionCreate = () => {

    
        const [ name_en , setNameEn ] = useState('')
        const [ name_es , setNameEs ] = useState('')
        const [ meaning_name_en , setMeaningNameEn ] = useState('')
        const [ meaning_name_es , setMeaningNameEs] = useState('')
        const navigate = useNavigate()    
        
    
    

        //Procedure Store
        const store = async (e) => {
            e.preventDefault()
            await LanguageModel.save('/api/languages', {
                name_en:name_en,
                name_es:name_es,
                meaning_name_en:meaning_name_en,
                meaning_name_es:meaning_name_es
            })
            navigate('/languages')
        }   

    return (
    <div className="row"  style={{margin: '0% 15%'}}>
        <div className="col-12">
            <div className="card">
                <div className="card-header"><h4>Create New Language</h4></div>
                <div className="card-body">
                    <form  onSubmit={store}>
                        <div className="row">
                            <div className="col-12 mb-2">
                                <div className="form-group">
                                    <label>Name In English</label>
                                    <input type="text" className="form-control" value={name_en} onChange={ (e)=> setNameEn(e.target.value)} />
                                </div>
                            </div>
                            <div className="col-12 mb-2">
                                <div className="form-group">
                                    <label>Name In Spanish</label>
                                    <input type="text" className="form-control"  value={name_es} onChange={ (e)=> setNameEs(e.target.value)} />
                                </div>
                            </div>
                            <div className="col-12 mb-2">
                                 <div className="form-floating">
                                    <textarea className="form-control" value={meaning_name_en} onChange={ (e)=> setMeaningNameEn(e.target.value)}></textarea>
                                    <label htmlFor="floatingTextarea2">Meaning Word In English</label>
                                </div>
                            </div>
                            <div className="col-12 mb-2">
                                <div className="form-floating">
                                    <textarea className="form-control"  value={meaning_name_es} onChange={ (e)=> setMeaningNameEs(e.target.value)}></textarea>
                                    <label htmlFor="floatingTextarea2">Meaning Word In Spanish</label>
                                </div>
                            </div>
                            <div className="col-12">
                                <button type="submit" className="btn btn-primary">Save</button>
                            </div>
                        </div>                        
                    </form>
                </div>
            </div>
        </div>
    </div>
    )
}

export default CompPermissionCreate