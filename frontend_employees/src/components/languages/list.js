import axios from 'axios'
import {useState, useEffect} from 'react'
import {Link} from 'react-router-dom'
import LanguageModel from '../../Models/Language'


const CompLanguageLists = () => {
    
    const [languages, setLanguage] = useState([])
    useEffect( ()=>{
        getLanguages()
    },[])

    //Procedure for show all Languages
    const getLanguages = async () => {
        const res = await LanguageModel.all('/api/languages')
        setLanguage(res.data);
    }

    //Procedure for delete Language 
    const deleteLanguage = async (id) => {
       await LanguageModel.remove('/api/languages/',id)
       getLanguages()
    }

    return(
        <div className="row"  style={{margin: "0% 20%"}}>
            <div className="col-12">             
                <div className="table-responsive">
                    <table className="table table-bordered">
                        <thead className="bg-primary text-white">
                            <tr>
                                <th>Name English</th>
                                <th>Name Spanish</th>
                                <th>Meaning English</th>
                                <th>Meaning Spanish</th>
                                <th>
                                    <Link to="/languages/create" className='btn btn-primary mt-2 mb-2'>Create</Link>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            { languages.map ( ( language) => (
                            <tr  key={ language.id }>
                                <td>{ language.name_en }</td>
                                <td>{ language.name_es }</td>
                                <td>{ language.meaning_name_en }</td>
                                <td>{ language.meaning_name_es }</td>
                                <td>
                                    <Link to={`/languages/edit/${ language.id }`} className='btn btn-info'>&#9997;</Link>
                                    <button onClick={ ()=>deleteLanguage( language.id ) } className='btn btn-danger'>&#9940;</button>
                                </td>
                            </tr>
                            )) }
                        </tbody>
                        
                    </table>
                </div>                          
            </div>
        </div>
    )

}

export default CompLanguageLists