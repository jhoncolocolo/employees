import { useEffect,useState } from 'react';
import { Link } from "react-router-dom";

const Nav = () =>{
    const [active, setActive] = useState(false)
    useEffect(() => {
        /*Add Script To Menu in Responsibe*/
        const scriptTag = document.createElement('script');

        scriptTag.src = "https://getbootstrap.com/docs/5.0/examples/offcanvas-navbar/offcanvas.js";
        scriptTag.async = true;

        document.body.appendChild(scriptTag);
        return () => {
            document.body.removeChild(scriptTag);
        }
    }, []);


    const closeMenu = () =>{
       const element = document.querySelector('#navbarsExampleDefault');
       if (element) {
          element.classList.remove('open');
        }
    }
    let menu;
        menu = (
            <div className="navbar-collapse offcanvas-collapse" id="navbarsExampleDefault">
                <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                    <li className="nav-item">
                      <Link to="/" onClick={closeMenu} className="navbar-brand">Home</Link>
                    </li>
                    <li className="nav-item">
                      <Link to="/users" onClick={closeMenu} className="navbar-brand">Users</Link>
                    </li>
                </ul>
                <ul className="navbar-nav ms-auto link-light">
                    <li className="nav-item">
                        <Link to="/login" className="navbar-brand">Login</Link>
                    </li>
                    <li className="nav-item">
                        <Link to="/register" className="navbar-brand">Register</Link>
                    </li>
                </ul>
            </div>
        )
    return (
        <nav className="navbar navbar-expand-lg fixed-top navbar-dark bg-dark" aria-label="Main navigation">
          <div className="container-fluid">
            <a className="navbar-brand" href="#">Learn With Colorado</a>
            <button className="navbar-toggler p-0 border-0" type="button" id="navbarSideCollapse" aria-label="Toggle navigation"  >
              <span  className="navbar-toggler-icon"></span>
            </button>
              {menu}
          </div>
        </nav>
    );
}

export default Nav;