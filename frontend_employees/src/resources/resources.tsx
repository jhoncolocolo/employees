import axios from 'axios'

const URI = 'http://localhost:8000/api/';
//console.log(localStorage.getItem('token'));
export const HTTP = axios.create({
    baseURL: URI,
    headers: { authorization: "Bearer " + localStorage.getItem('token') }
});