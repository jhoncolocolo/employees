export interface UserResponse {
    id:                  number;
    area_company:        string;
    country:             string;
    identification_type: string;
    identification:      string;
    email:               string;
    employee:            string;
    register:            Date;
    status:              string;
};

export interface UserInterface {
    id:                     number;
    area_id:                number;
    country_id:             number;
    identification_type_id: number;
    identification_number:  string;
    user_name:              string;
    email:                  string;
    first_name:             string;
    other_names:            string;
    surname:                string;
    second_surname:         string;
    register:               Date;
    status:                 string;
};

export interface UserResponseData {
    current_page:   number;
    data:           UserData[];
    first_page_url: string;
    from:           number;
    last_page:      number;
    last_page_url:  string;
    links:          Link[];
    next_page_url:  string;
    path:           string;
    per_page:       number;
    prev_page_url:  null;
    to:             number;
    total:          number;
}

export interface UserData {
    id:                  number;
    area_company:        string;
    country:             string;
    identification_type: string;
    identification:      string;
    email:               string;
    employee:            string;
    register:            Date;
    status:              string;
}

export interface Link {
    url:    null | string;
    label:  string;
    active: boolean;
}