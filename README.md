# Employess Full Stack
Full Stack Laravel React For Employees

## Run Laravel Backend Project

##DEMO

[Demo Application Employees](https://1drv.ms/v/s!Al167hgOrzsSgl9kM-sOGaPM6bH8?e=AqDzG7)


Clone the project

```bash
  git clone https://gitlab.com/jhoncolocolo/employees.git
```

Go to the project directory

```bash
  cd Employees
```

Install dependencies

```bash
  composer install
```

## Environment Variables
For this example, the database name is: pet_vinixcode but if you want to change the name, put the name of your preference in the correct parameter of the .(env) file

In the environment (.env) file, update these variables, remember you need to rename the file in the root folder .env.example to .env



`DB_CONNECT=mysql`

`DB_HOST=127.0.0.1`

`DB_PORT=3306`

`DB_DATABASE=backend_employees`

`DB_USERNAME=YOUR_USER`


After running these migrations:

```bash
php artisan migrate:refresh --seed
```

Start the server

```bash
  php artisan serve
```

  
  ## Explanation Backend Code

[Basic explanation of Backend Code](https://1drv.ms/v/s!Al167hgOrzsSgl7oThU67WocjAm_?e=KbjLbY)

  

/***********************************************************************************/


## Run Vue Frontend Project

In the same folder that we already downloaded called Employees, we will now navigate to the subfolder called frontend_employees

```bash
  cd frontend_employees
```

Install dependencies

```bash
  npm install
```

Start the server

```bash
  npm start
```

[Basic explanation of Frontend Code](https://1drv.ms/v/s!Al167hgOrzsSgl0ORi4Z5MlIFw1s?e=rmLuRh)


## Tech Stack

**Client:** React, Boostrap 5

**Server:** PHP (LARAVEL)


## Authors

- [@jhoncolocolo](https://github.com/jhoncolocolo)

## Acknowledgements
- [@cidenet](https://www.cidenet.com.co/)
- For challenging me to grow as a developer 
